var uuid = require('node-uuid')
  , service = require('./app/service/buildconfig')
  , fs = require('fs');

/*global module:false*/
module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-node-inspector');
  grunt.loadNpmTasks('grunt-replace');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    meta: {
      banner: '/* Copyrite <%= pkg.name %> */'
    },

    clean: ['app/public/css/**', 'app/public/js/**', 'app/public/fonts/**'],

    copy: {
      css: {
        expand: true,
        cwd: 'app/public_source/css',
        src: '**/*.css',
        dest: 'app/public/css'
      },
      js: {
        expand: true,
        cwd: 'app/public_source/js/app',
        src: '**/*.js',
        dest: 'app/public/js'
      },
      fonts: {
        expand: true,
        cwd: 'app/public_source/fonts',
        src: '**/*',
        dest: 'app/public/fonts'
      }
    },

    'node-inspector': {
      app: {}
    },

    nodemon: {
      dev: {
        script: 'app/app.js',
        options: {
          nodeArgs: ['--debug'],
          ignore: ['README.md', 'currentbuild.json','app/public_source/**', '.git/**', 'app/public/**', '*.jade', 'app/temp/**', 'app/resources**', '*.aof', '*.rdb']
        }
      }
    },

    uglify: {
      vendor: {
          src: ['<banner:meta.banner>'
            ,  'app/public_source/js/vendor/jquery.js'
            , 'app/public_source/js/vendor/sugar-1.4.0.min.js'
            , 'app/public_source/js/vendor/form.jquery.js'
            , 'app/public_source/js/vendor/jansy-fileupload.js'
            , 'app/public_source/js/vendor/bootstrap/tooltip.js'
            , 'app/public_source/js/vendor/bootstrap/affix.js'
            , 'app/public_source/js/vendor/bootstrap/alert.js'
            , 'app/public_source/js/vendor/bootstrap/button.js'
            , 'app/public_source/js/vendor/bootstrap/carousel.js'
            , 'app/public_source/js/vendor/bootstrap/collapse.js'
            , 'app/public_source/js/vendor/bootstrap/dropdown.js'
            , 'app/public_source/js/vendor/bootstrap/fileinput.js'
            , 'app/public_source/js/vendor/bootstrap/modal.js'
            , 'app/public_source/js/vendor/bootstrap/popover.js'
            , 'app/public_source/js/vendor/bootstrap/transition.js'
          ],
          dest: 'app/public/js/app-vendor.js'
      },

      app: {
        options: {
          mangle: true,
          compress: true,
          sourceMap: true,
          sourceMapIncludeSources: true,
          sourceMapName: 'app/public/js/source-map.js',
          banner: '/*! <%= pkg.name %> - v<%= meta.buildId %> - ' +
            '<%= grunt.template.today("yyyy-mm-dd") %> */'
        },
        src: ['<banner:meta.banner>',
          'app/public_source/js/app/**/*.js'
        ],
        dest: 'app/public/js/<%= meta.buildId %>.js'
      }
    },

    replace: {
      dist: {
        options: {
          variables: {
            'buildId': '<%= meta.buildId %>'
          }
        },
        files: [
          {
            src: ['app/views/layout_src.jade'],
            dest: 'app/views/layout.jade'
          }
        ]
      }
    },

    less: {
      development: {
        options: {
          yuicompress: true
        },
        files: {
          "app/public/css/<%= meta.buildId %>.css": "app/public_source/css/imports.less"
        }
      }
    },

    jade: {
      frontend: {
        options: {
          compileDebug: false,
          namespace: 'App.templates',
          client: true,
          data: {
            debug: false
          },
          processName: function(str) { return str.match(/^app\/views\/shared\/(.*)\.jade$/)[1]; }
        },
        files: {
          'app/public/js/templates-<%= meta.buildId %>.js': 'app/views/shared/*.jade'
        }
      }
    },

    watch: {
      options: {
        livereload:true,
        nospawn: true
      },
      javascript: {
        files: 'app/public_source/js/app/**/*.js',
        tasks: ['UpdateJS']
      },
      layout_src: {
        files: 'app/views/layout_src.jade',
        tasks: ['UpdateLayout']
      },
      css: {
        files: 'app/public_source/css/*.less',
        tasks: ['UpdateCSS']
      },
      jade: {
        files: 'app/views/shared/*.jade',
        tasks: ['UpdateJade']
      }
    },

    concurrent: {
      watchers: {
        tasks: ['nodemon', 'node-inspector', 'watch'],
        options: {
          logConcurrentOutput: true
        }
      },
      compilers: {
        tasks: ['jade:compile', 'uglify', 'less'],
        options: {
          logConcurrentOutput: true
        }
      }
    }
  });

  grunt.registerTask('UpdateLayout', function() {
    var buildConfig = service.getConfig();
    grunt.config.set('replace.dist.options.variables', buildConfig);
    grunt.task.run(['replace']);
  });

  grunt.registerTask('UpdateJS', function() {
    var buildConfig = service.getConfig();
    buildConfig.js = uuid.v4();
    grunt.config.set('replace.dist.options.variables', buildConfig);

    grunt.config.set('meta.buildId', buildConfig.js);
    grunt.task.run(['uglify:app', 'replace', 'copy']);
    service.saveConfig(buildConfig);
  });

  grunt.registerTask('UpdateCSS', function() {
    var buildConfig = service.getConfig();
    buildConfig.css = uuid.v4();
    grunt.config.set('replace.dist.options.variables', buildConfig);

    grunt.config.set('meta.buildId', buildConfig.css);
    grunt.task.run(['less', 'replace', 'copy']);
    service.saveConfig(buildConfig);
  });

  grunt.registerTask('UpdateJade', function() {
    var buildConfig = service.getConfig();
    buildConfig.jade = uuid.v4();
    grunt.config.set('replace.dist.options.variables', buildConfig);

    grunt.config.set('meta.buildId', buildConfig.jade);
    grunt.task.run(['jade:frontend', 'replace']);
    service.saveConfig(buildConfig);
  });

  grunt.registerTask('default', 'Default dev task', function() {
    var buildConfig = service.getConfig();
    var buildid = uuid.v4();

    buildConfig.css = buildid;
    buildConfig.js = buildid;
    buildConfig.jade = buildid;

    grunt.config.set('meta.buildId', buildid);
    grunt.config.set('replace.dist.options.variables', buildConfig);

    fs.writeFileSync('currentbuild.json', JSON.stringify(buildConfig));
    grunt.task.run(['clean','jade:frontend', 'uglify', 'less' , 'replace', 'copy','concurrent:watchers']);
  });
};

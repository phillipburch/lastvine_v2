var site = require('../controllers');

module.exports = function (app, passport, auth) {
  //Auth
  app.get('/', site.decorate(site.home.home));
  app.get('/login', site.decorate(site.auth.signin));
  app.get('/signup', site.decorate(site.auth.signup));
  app.get('/user/signup2', site.decorate(site.auth.signup2));
  app.get('/user/logout', site.decorate(site.auth.logout));

  // User
  app.get('/user/settings', site.decorate(site.user.settings));
  app.get('/user/:id', site.decorate(site.user.show));
  app.get('/user/:id/edit', site.decorate(site.user.edit));
  app.get('/unsubscribe', site.decorate(site.user.unsubscribe));

  //Search
  app.get('/search', site.search.search);

  // Social
  app.get('/verify/facebook', passport.authenticate('facebook'));
  app.get('/facebook', site.social.facebookRedirects );
  app.get('/facebook/getFriends', site.social.getFBFriends );
  app.get('/facebook/step2', site.social.step2 );
  app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { successRedirect: '/verify/user/?facebookSuccess',
                                        failureRedirect: '/login' }));
  app.get('/auth/twitter/callback',
    passport.authenticate('twitter', { successRedirect: '/?twittersuccess=1',
                                       failureRedirect: '/login' }));

  app.get('/sitemap.xml', site.sitemap.index );
};
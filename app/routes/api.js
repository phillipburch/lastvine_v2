var api = require('../api');

module.exports = function (app, passport, auth) {
  app.post('/api/user/create', api.decorate(api.auth.create));
  app.post('/api/user/signup', api.decorate(api.auth.create));
  app.post('/api/phone/signup', api.decorate(api.auth.phoneCreate));
  app.post('/api/user/login',
    passport.authenticate('local', {
      failureRedirect: '/login'
    }), api.decorate(api.auth.login));
  app.get('/api/user/logout', api.decorate(api.auth.logout));

  // User
  app.get('/api/user', api.decorate(api.user.list));
  app.get('/api/user/settings', api.decorate(api.user.settings));
  app.post('/api/user/image', api.user.uploadImage);

  app.get('/user/:id', api.decorate(api.user.show));
  app.get('/api/user/:id', api.decorate(api.user.show));

  app.get('/user/:id/edit', api.decorate(api.user.edit));
  app.post('/api/unsubscribe', api.decorate(api.user.saveUnsubscribe));

  // Uploads
  app.post('/api/asset/upload', api.imageAsset.upload);

  //Search
  app.get('/api/search', api.search.search);
};
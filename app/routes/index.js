var api = require('../api'),
  site = require('../controllers');

module.exports = {
  api: api,
  site: site
}
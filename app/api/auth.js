
/*
 * GET users listing.
 */

var mongoose = require('mongoose'),
  redis = require('redis'),
  w = require('when'),
  imageService = require('../service/image'),
  uuid = require('node-uuid'),
  validator = require('validator'),
  emailService = require('../service/email'),
  User = mongoose.model('User');

var controller = {
  phoneCreate: function(phone, password) {
    var self = this;
    return User.create({
      phone: phone,
      password: password,
      provider: 'phone',
      token: uuid.v4()
    }).then(function(newUser) {
        return w.promise(function(resolve) {
          self.req.logIn(newUser, function() {
            emailService.newUserReport(newUser);
            resolve({template: '', model: newUser});
          });
        });
      }, function(err) {
        return {
          templage: 'auth/signup',
          model: {
            errors: err.errors || err,
          }
        };
      });
  },

  uploadImage: function(req, res) {
    var data = req.user;
    var imageId = 'user_' + req.user._id;
    imageService.save(imageId, uuid.v4(), 300, null, req.files.upload,
      function(url) {
        data.thumbnailUrl = url;
        data.save(function (err, item) {
          res.json(item.thumbnailUrl);
        });
      });
  }
};

module.exports = controller;
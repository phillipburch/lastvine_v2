var _ = require('underscore'),
  w = require('when'),
  services = require('../service'),
  decorate;

function extractArgumentMap(functionCode) {
  var argumentStringMatch = functionCode.match(new RegExp('\\([^)]*\\)', ''));

  var argumentMap = argumentStringMatch[0].match(new RegExp('[^\\s,()]+', 'g'));

  if(!argumentMap || !argumentMap.length)
    return([]);

  return(argumentMap);
}

function invoke (method, namedArguments, context) {
  var orderedArguments = [];

  method.argumentMap = extractArgumentMap(method.toString());

  for (var i = 0 ; i < method.argumentMap.length ; i++){
    if (method.argumentMap[ i ] in namedArguments){
      orderedArguments.push(namedArguments[method.argumentMap[i]]);
    } else {
      orderedArguments.push(null);
    }
  }

  return method.apply(context, orderedArguments);
}

function hydrateUser (token, res) {
  if(!token)
    return w({});

  return services.user.getByToken(token).then(function(user) {
    if(user) {
      return user;
    } else {
      res.json(401, {msg: 'Invalid access token.'});
    }
  }).catch(function() {
    res.json(401, {msg: 'Invalid access token.'});
  });
}

decorate = function (method) {
  return function (req, res) {
    var token = req.body.token || req.query.token;

    hydrateUser(token, res).then(function(user) {
      if(user)
        req.user = user;

      var options = _.extend(req.body, req.files, req.query, req.params),
        context = {
          req: req,
          res: res,
          user: req.user
        };

      invoke(method, options, context).then(function (result) {
        if(result.redirect)
          return res.json(result);

        res.json(result.model);
      }, function (error) {
        var errorCode = error.errorCode || 500;
        res.json(errorCode, error);
      });
    });
  };
};


module.exports = {
  auth: require('./auth'),
  search: require('./search'),
  imageAsset: require('./imageAsset'),
  user: require('./user'),
  decorate: decorate
};

/*
 * GET users listing.
 */

var mongoose = require('mongoose')
  , redis = require('redis')
  , w = require('when')
  , nodefn = require('when/node')
  , uuid = require('node-uuid')
  , imageService = require('../service/image')
  , userService = require('../service/user')
  , elasticSearchClient = require('elasticsearchclient')
  , emailService = require('../service/email')
  , validator = require('validator')
  , User = mongoose.model('User');

var controller = {
  list: function(){
    var self = this;
    return User.find({}).exec().then(function(users) {
      return {
        template: 'user/list',
        model: users
      };
    });
  },

  saveUnsubscribe: function(email) {
    var client = redis.createClient(),
      key = 'unsubscribed:email';

    return nodefn.call(client.sadd.bind(client), key, 'email')
      .then(function(response) {
        return {model: {success: !!response}};
      }, function(err) {
        return err;
      });
  },


  recoverPassword: function(token) {
    var client = redis.createClient(),
      key ='forgotpassword:' + token;

    return nodefn.call(client.get.bind(client), key)
      .then(function(email) {
        if(!email)
          return {redirect: '/'};

        return {template: 'user/recoverpassword', model: {email: email, token: token}};
      }, function(err) {
        return err;
      });
  },

  resetPassword: function(req, res) {
    if(!req.body.newpassword)
      return res.json({success: false, field: 'newpassword'});
    var client = redis.createClient();
    var token  = req.params.token;
    client.get('forgotpassword:' + token, function(err, email) {
      if(!email)
        return redirect(res, '/');
      User.findOne({ email: email.toLowerCase() }, function (err, user) {
        user.password = req.body.newpassword;
        user.save(function() {
          client.del('forgotpassword:' + token, redis.print);
          res.json({success: true});
        });
      });
    });
  },

  verifyEmail: function(req, res) {
    if(!validator.isEmail(req.body.email))
      return res.json({success: false, message: 'Not a valid email address.', field: 'email'});

    User.findOne({ email: req.body.email.toLowerCase() }, function (err, user) {
      if(user) {
        var client = redis.createClient();
        var token = uuid.v4();
        client.set('forgotpassword:' + token, user.email, 'EX', 3 * 60 * 60, function(err, repsonse) {
          if(!err){
            emailService.recoverPassword(user.email, token);
          }
          res.json({success: true});
        });
      } else {
        res.json({success: false, message: 'Email address not found.'});
      }
    });
  },

  update: function(req, res) {
    req.user.name = req.body.name;
    req.user.email = req.body.email;
    req.user.denomination = req.body.denomination;
    req.user.save(function() {
      respond(res, null, {url: '/user/' + req.user._id });
      var searchClient = new elasticSearchClient(AppConfig.elasticSearch);
      searchClient.index('lastvine', 'user', {
        name: req.user.name,
        _id: req.user._id
      }, req.user._id.toString())
      .exec();
    });
  },
/*
  following: function(req, res) {
    var client = redis.createClient();
    var multi = client.multi();
    var followerCount, followingCount, isFollowee, me=this;
    //bibleService.hydrateActivityByUser(req.params.id, function(readingLog) {
      User.findOne({_id: req.params.id}).exec(function(err, user) {
        multi.zcard('following:' + req.params.id, function(err, response) {
          followingCount = response
        })
        multi.zcard('user:followers:' + req.params.id, function(err, response) {
          followerCount = response
        })
        multi.zrank('user:followers:' + user._id, req.user._id, function(err, response) {
          isFollowee = Object.isNumber(response)
        })

        multi.zrange('following:' + req.params.id, 0, 20, function(err, response){
          me.getUsersByIds(response, function(users) {
            respond(res, 'user/show', {
              item: user,
              tab: 'following',
              followerCount: followerCount,
              followingCount: followingCount,
              isFollowee: isFollowee,
              following: users,
              readingLog: readingLog
            });
          });
        })
        multi.exec();
      })
    //});
  },

  followers: function(req, res) {
    var client = redis.createClient();
    var multi = client.multi();
    var followerCount, followingCount, isFollowee, me = this;
    bibleService.hydrateActivityByUser(req.params.id, function(readingLog) {
      User.findOne({_id: req.params.id}).exec(function(err, user) {
        multi.zcard('following:' + req.params.id, function(err, response) {
          followingCount = response
        })
        multi.zcard('user:followers:' + req.params.id, function(err, response) {
          followerCount = response
        })

        multi.zrank('user:followers:' + user._id, req.user._id, function(err, response) {
          isFollowee = Object.isNumber(response)
        })

        multi.zrange('user:followers:' + req.params.id, 0, 20, function(err, response){
          me.getUsersByIds(response, function(users) {
            respond(res, 'user/show', {
              item: user,
              tab: 'followers',
              followerCount: followerCount,
              followingCount: followingCount,
              isFollowee: isFollowee,
              followers: users,
              readingLog: readingLog
            })
          })
        })

        multi.exec();
      })
    })
  },


  getUsersByIds: function(ids, cb) {
    User.find({_id: {'$in': ids}}, {name: 1, thumbnailUrl: 1}).exec(function(err, users) {
      cb(users)
    })
  },*/

  favorites: function(req, res) {
    var client = redis.createClient();
    var multi = client.multi();
    var followerCount, followingCount, isFollowee, me = this;
    User.findOne({_id: req.params.id}).exec(function(err, user) {
      multi.zcard('following:' + req.params.id, function(err, response) {
        followingCount = response;
      });
      multi.zcard('user:followers:' + req.params.id, function(err, response) {
        followerCount = response;
      });
      multi.zrank('user:followers:' + user._id, req.user._id, function(err, response) {
        isFollowee = Object.isNumber(response);
      });

      multi.exec(function() {
        respond(res, 'user/show', {
          item: user,
          followerCount: followerCount,
          followingCount: followingCount,
          tab: 'favorites',
          isFollowee: isFollowee,
          posts: []
        });
      });
    });
  },

  show: function(id) {
    var client = redis.createClient();
    var multi = client.multi();
    var followerCount, followingCount, me, isFollowee;

    return userService.getUser(id).then(function(user) {
      return{
        template: 'user/show',
        model: {
          item: user,
          posts: [],
          tab: 'posts'
        }
      };
    }, function(err) {
      return {
        error: err
      };
    });
  },

  uploadImage: function(req, res) {
    var data = req.user;
    var me = this;
    data.thumbnailUrl = req.body.thumbnailUrl;
    data.save(function (err, item) {
      res.json(item.thumbnailUrl);
    });
  },

  settings: function() {
    var self = this;
    if(!this.user)
      return w.reject({errorCode: 401, message: 'You have to be logged in'});

    return w({template: 'user/show', model: {
      item: self.user
    }});
  }
};

module.exports = controller;
/**
 * Module dependencies.
 */

var express = require('express'),
  http = require('http'),
  colors = require('colors'),
  uuid = require('node-uuid'),
  https = require('https'),
  passport = require('passport'),
  fs = require('fs'),
  sugar = require('sugar'),
  flash = require('connect-flash'),
  mongoose = require('mongoose'),
  connect = require('connect'),
  viewHelpers = require('./config/middlewares/view'),
  RedisStore = require('connect-redis')(express),
  auth = require('./config/middlewares/authorization'),
  cluster = require('cluster'),
  path = require('path');

var redis = require('redis').createClient();
redis.on('error', function() {});

// Code to run if we're in the master process
if (cluster.isMaster && process.env.NODE_ENVIRONMENT !== 'Development') {
  // Count the machine's CPUs
  var cpuCount = require('os').cpus().length;

  // Create a worker for each CPU
  for (var i = 0; i < cpuCount; i += 1) {
    cluster.fork();
  }
} else {
  var initCallbacks = [];
  GLOBAL.onIOInitiated = function(cb) {
    if(cb) {
      initCallbacks.push(cb);
    } else {
      initCallbacks.each(function(c) {
        c.call();
      });
    }
  };

  process.env.NODE_ENVIRONMENT = process.env.NODE_ENVIRONMENT || 'production';
  GLOBAL.AppConfig = require('./config/config')[process.env.NODE_ENVIRONMENT.toLowerCase()];

  var imageService = require('./service/image');
  // Bootstrap db connection
  console.log(GLOBAL.AppConfig.db);
  var db = mongoose.connect(GLOBAL.AppConfig.db);
  var app = express();
  //Bootstrap models
  var modelsPath = __dirname + '/models';

  fs.readdirSync(modelsPath).forEach(function (file) {
    if (fs.statSync(path.join(modelsPath, file)).isFile() && file !== 'base.js') {
      require(modelsPath+'/'+file)(mongoose);
    }
  });

  var FileUpload = require('./config/middlewares/fileupload'),
    sessionStore,
    cookieParser = express.cookieParser();

  // bootstrap passport config
  require('./config/passport')(passport, GLOBAL.AppConfig);

  app.configure(function(){
    app.set('port', process.env.SCHEJULEPORT || 3010);
    app.use(function (req, res, next) {
      if ('/robots.txt' === req.url) {
        res.type('text/plain');
        res.send('User-agent: *\nDisallow: /');
      } else {
        next();
      }
    });
    app.use(express.static(path.join(__dirname, 'public')));
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');

    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(cookieParser);

    // dynamic helpers
    app.use(viewHelpers(GLOBAL.AppConfig));

    //app.use(express.bodyParser({defer: true}));

    app.use(express.json());
    app.use(express.urlencoded());

    app.use(express.methodOverride());

/*    app.use(function(req, res, next) {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
      res.header('Access-Control-Allow-Headers', 'Content-Type');

      next();
    });
*/
    // Session-less. Apparently, you can't have async stuff here.
    // https://github.com/felixge/node-formidable/issues/34
    app.use(FileUpload.handleUploads);

    sessionStore = new RedisStore({ host: 'localhost', port: 6379, client: redis });
    // express/mongo session storage
    app.use(express.session({
      secret: 'noobjs',
      store: sessionStore
    }));

     // use passport session
    app.use(passport.initialize());
    app.use(passport.session());

    app.use(app.router);

    app.use(function(req, res, next){
      res.status(404);
      // respond with html page
      if (req.accepts('html')) {
        res.render('404', { url: req.url });
        return;
      }

      // respond with json
      if (req.accepts('json')) {
        res.send({ error: 'Not found' });
        return;
      }

      // default to plain-text. send()
      res.type('txt').send('Not found');
    });
  });

  // Bootstrap routes
  require('./routes/site')(app, passport, auth);
  require('./routes/api')(app, passport, auth);

  app.configure('development', function(){
    app.use(express.errorHandler());
  });

  var server = http.createServer(app).listen(app.get('port'), function(){
    var msg = 'Server listening on port: ' + app.get('port') + '.';
    console.log(msg.magenta);

    var infoMessage = 'App is currently running in '.yellow +
      process.env.NODE_ENVIRONMENT.bold.cyan +
      ' mode. To change this, update the NODE_ENVIRONMENT value in your bash profile.'.yellow;

    console.log(infoMessage);
  });

  // bootstrap passport config
  GLOBAL.io = require('./config/socketio')(server, sessionStore, cookieParser, passport);
  GLOBAL.onIOInitiated();
  process.on('uncaughtException', function (err) {
    console.log(err);
  });
}


// Listen for dying workers
cluster.on('exit', function (worker) {
  // Replace the dead worker,
  // we're not sentimental
  console.log('Worker ' + worker.id + ' died :(');
  cluster.fork();
});
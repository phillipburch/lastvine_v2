module.exports = function (mongoose) {
  var Schema = mongoose.Schema, 
    baseSchema = require('./base');

  var callSchema = new Schema({
    duration: Number,
    reservation: [{type: Schema.Types.ObjectId, ref: 'Reservation' }],
    respnseCode: String
  });

  callSchema.plugin(baseSchema);

  mongoose.model('CallLog', callSchema);
};
  module.exports = function (mongoose) {
    var Schema = mongoose.Schema
    , baseSchema = require('./base');

  var followSchema = new Schema({
    followee: { type: Schema.Types.ObjectId, ref: 'User' }
    ,follower: { type: Schema.Types.ObjectId, ref: 'User' }
  });

  followSchema.plugin(baseSchema);

  mongoose.model('Follow', followSchema);
};
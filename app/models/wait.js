  module.exports = function (mongoose) {
    var Schema = mongoose.Schema
    , baseSchema = require('./base');

  var waitSchema = new Schema({
    email: String
  });

  waitSchema.plugin(baseSchema);

  mongoose.model('Wait', waitSchema);
};
module.exports = exports = function lastModifiedPlugin (schema) {
  schema.add({ lastMod: Date })
  schema.add({
  	createdOn: {
			type: Date,
			default: Date.now
		}
	});

  schema.pre('save', function (next) {
    this.lastMod = new Date
    next()
  })
}
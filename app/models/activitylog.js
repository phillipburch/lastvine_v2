module.exports = function (mongoose) {

  var Schema = mongoose.Schema, 
    baseSchema = require('./base');

  var ActivitySchema = new Schema({
    osis: String,
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    ordinal: Number,
    type: String,
    comments: [{
      user: { type: Schema.Types.ObjectId, ref: 'User' },
      text: String,
      createDt: Date,
      likes: [{ type: Schema.Types.ObjectId, ref: 'User' }]
    }],
    likes: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    likeCount: {type: Number,default: 0}
  });

  ActivitySchema.plugin(baseSchema);

  mongoose.model('ActivityLog', ActivitySchema);
};
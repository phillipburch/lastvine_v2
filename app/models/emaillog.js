module.exports = function (mongoose) {
  var Schema = mongoose.Schema
      , baseSchema = require('./base');

  var emailSchema = new Schema({
      sender: { type: Schema.Types.ObjectId, ref: 'User' },
      reciever: String
  });

  emailSchema.plugin(baseSchema);

  mongoose.model('EmailLog', emailSchema);
}
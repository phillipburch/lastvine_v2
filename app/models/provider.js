module.exports = function (mongoose) {
  var Schema = mongoose.Schema,
    baseSchema = require('./base');

  var ProviderSchema = new Schema({
    type: String,
    averageDuration: Number,
    avgRating: Number,
    reviews: [{rating: Number, text: String, user: String, userId: String}],
    loc: [Number, Number],
    history: [{}]
  });

  ProviderSchema.plugin(baseSchema);
  ProviderSchema.index({'loc': '2d'});
  
  mongoose.model('Provider', ProviderSchema);
};
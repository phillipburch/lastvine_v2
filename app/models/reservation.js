module.exports = function (mongoose) {
  var Schema = mongoose.Schema,
    baseSchema = require('./base');

  var ReservationSchema = new Schema({
    type: String,
    provider: [{type: Schema.Types.ObjectId, ref: 'Provider' }],
    estimatedTime: Date,
    user: [{type: Schema.Types.ObjectId, ref: 'User' }],
    loc: [Number, Number]
  });

  ReservationSchema.plugin(baseSchema);
  ReservationSchema.index({'loc': '2d'});
  
  mongoose.model('Reservation', ReservationSchema);
};
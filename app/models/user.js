module.exports = function (mongoose) {
  var Schema = mongoose.Schema,
  crypto = require('crypto'),
  baseSchema = require('./base'),
  authTypes = ['github', 'twitter', 'facebook', 'google', 'phone'];

  var userSchema = new Schema({
    name: String,
    email: {type: String, index: {unique: true, dropDups: true}},
    signupdate: Date,
    hashedPassword: String,
    salt: String,
    avatar: String,
    phone: String,
    city: String,
    images: [],
    state: String,
    score: Number,
    unreadNotificationCount: {type: Number, default: 0},
    zip: String,
    token: String,
    followCount: Number,
    tag: {type: String, default: 'none'},
    thumbnailUrl: {type: String, default: 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image'},
    squareImgUrl: {type: String, default: 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image'},
    smsCode: String,
    phoneVerified: Boolean,
    provider: String,
    facebookVerified: Boolean,
    facebookToken: String,
    facebookId: String,
    stripeCustomerId: String,
    activeCards: [],
    favorites: [{osis: String}],
    groups: [{ type: Schema.Types.ObjectId, ref: 'Group' }],
    following: [{
      assetName: String,
      assetId: String
    }],
    loc: [Number, Number]
  });

  userSchema.index({'loc': '2d'});
  userSchema.path('email').index({ unique: true });

  // virtual attributes
  userSchema.virtual('password').set(function(password) {
      this._password = password;
      this.salt = this.makeSalt();
      this.hashedPassword = this.encryptPassword(password);
    }).get(function() { return this._password; });

  // validations
  var validatePresenceOf = function (value) {
    return value && value.length;
  };

  // the below 4 validations only apply if you are signing up traditionally

  userSchema.path('name').validate(function (name) {
    // if you are authenticating by any of the oauth strategies, don't validate
    if (authTypes.indexOf(this.provider) !== -1) return true;
    return name.length;
  }, 'Name cannot be blank');

  userSchema.path('email').validate(function (email) {
    // if you are authenticating by any of the oauth strategies, don't validate
    if (authTypes.indexOf(this.provider) !== -1) return true;
    return email.length;
  }, 'Email cannot be blank');


  userSchema.path('hashedPassword').validate(function (hashedPassword) {
    // if you are authenticating by any of the oauth strategies, don't validate
    if (authTypes.indexOf(this.provider) !== -1) return true;
    return hashedPassword.length;
  }, 'Password cannot be blank');

  // pre save hooks
  userSchema.pre('save', function(next) {
    if(this.provider === 'phone')
      return next();

    this.email = this.email.toLowerCase();

    if (!validatePresenceOf(this.password) && authTypes.indexOf(this.provider) === -1)
      return next(new Error('Invalid password.'));

    next();
  });

  // methods
  userSchema.method('authenticate', function(plainText) {
    return this.encryptPassword(plainText) === this.hashedPassword;
  });

  userSchema.method('makeSalt', function() {
    return Math.round((new Date().valueOf() * Math.random())) + '';
  });

  userSchema.method('encryptPassword', function(password) {
    if (!password || !this.salt) return '';
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
  });

  userSchema.plugin(baseSchema);

  mongoose.model('User', userSchema);
};

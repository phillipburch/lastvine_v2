App.Views.User = App.View.extend({

  init: function (argument) {
    this._super();
    $(this.elm).on('mouseenter', '.btn.following', function() {
      $('.isUnfollow').val("true")
      $(this).addClass('btn-danger').html('Unfollow')
    });
    $(this.elm).on('mouseleave', '.btn.following', function() {
      $('.isUnfollow').val("false")
      $(this).removeClass('btn-danger').html('Following')
    })
    $("form.followForm").on('success', function() {
      if($('button', this).hasClass('following')) {
        $('button', this).removeClass('following').addClass('followMe').addClass('btn-success').removeClass('btn-danger').html('Follow');
        $('.followForm [ name="isUnfollow"]').val('false')
      } else {
        $('button', this).removeClass('followMe').addClass('following').removeClass('btn-success').html('Following');
        $('.followForm [ name="isUnfollow"]').val('true')
      }
    })
    $('[name="thumbnailUrl"]').on('change', function(e) {
      App.User.thumbnailUrl = $(this).val();
      App.ajax.post('/user/image', {thumbnailUrl: $(this).val()}, function() {})
    })
  }
})
App.Views.Auth = App.View.extend({
  events: {
    'keyup input': 'typing'
  },

  typing: function(e) {
    $(e.target).removeClass('error')
  },

  init: function() {
    this._super();

    $(this.elm).on('success', function(e, response) {

      if(response.isRedirect)
        location.href = response.url;

      if(!response.success){
        $('[name="' + response.field + '"]').addClass('error');
        $('.alert.alert-danger').removeClass('hide').find('.message').html(response.message)
      } else {
        location.href='/group/new?isnew=1';
      }
    })
  }

});
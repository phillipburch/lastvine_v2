App.Models.user = App.BaseModel.extend({
  entity: 'user',
  fetchFollowing: function(id, cb) {
    var token = "";
    if(App.User)
        token = App.User.token;
    $.get('/api/user/' + id +'/following/?access_token=' + token , cb)
  },
  fetchFollowers: function(id, cb) {
    var token = "";
    if(App.User)
        token = App.User.token;
    $.get('/api/user/' + id +'/followers/?access_token=' + token , cb)
  },
  fetchFavorites: function(id, cb){
    var token = "";
    if(App.User)
        token = App.User.token;
    $.get('/api/user/'+ id +'/favorites/?access_token=' + token , cb)
  }
})

App.Controllers.user = App.BaseController.extend({
  routes: {
    '/user/signup{?query}': 'signup',
    '/login{?query}': 'login',
    '/login': 'login',
    '/user/{id}': 'show',
    '/user/{id}/following': 'following',
    '/user/{id}/followers': 'followers',
    '/user/{id}/favorites': 'favorites'
  },

  signup: function(query) {
    if(App.User)
      return crossroads.navigator.nagivate('/')
    this._showPage('_signup', {isEmail: true, title: (query.next) ? "Before creating your group, let us know who you are." : "Sign Up", next: query.next})
    var view = new App.Views.Auth({
      elm: $('form')
    })
  },

  login: function(query) {
    if(App.User)
      return crossroads.navigator.nagivate('/')

    this._showPage('_signin', {title: "Sign in"})
    var view = new App.Views.Auth({
      elm: $('form')
    })
  },

  followers: function(id) {
    var model = new App.Models.user();
    var me = this;
    model.fetchFollowers(id, function(response) {
      me._showPage('_usershow', response)
      var view = new App.Views.User({
        elm: $('.user-page')
      })
    })
  },

  following: function(id) {
    var model = new App.Models.user();
    var me = this;
    model.fetchFollowing(id, function(response) {
      me._showPage('_usershow', response)
      var view = new App.Views.User({
        elm: $('.user-page')
      })
    })
  },

  favorites: function(id) {
    var model = new App.Models.user();
    var me = this;
    model.fetchFavorites(id, function(response) {
      me._showPage('_usershow', response)
      var view = new App.Views.User({
        elm: $('.user-page')
      })
    })
  },
  show: function(id) {
    var model = new App.Models.user();
    var me = this;
    model.fetchOne(id, function(response) {
      me._showPage('_usershow', response)
      var view = new App.Views.User({
        elm: $('.user-page')
      })
    })
  }
});

new App.Controllers.user();
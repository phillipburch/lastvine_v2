/* Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 */
// Inspired by base2 and Prototype
var App = {};
(function(){
  var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;
  // The base Class implementation
  Class = function () { };

  // Create a new Class that inherits from this class
  Class.extend = function(prop) {
    var _super = this.prototype;

    // Instantiate a base class (but only create the instance,
    // don't run the init constructor)
    initializing = true;
    var prototype = new this();
    initializing = false;

    // Copy the properties over onto the new prototype
    for (var name in prop) {
      // Check if we're overwriting an existing function
      prototype[name] = typeof prop[name] == "function" &&
        typeof _super[name] == "function" && fnTest.test(prop[name]) ?
        (function(name, fn){
          return function() {
            var tmp = this._super;

            // Add a new ._super() method that is the same method
            // but on the super-class
            this._super = _super[name];

            // The method only need to be bound temporarily, so we
            // remove it when we're done executing
            var ret = fn.apply(this, arguments);
            this._super = tmp;

            return ret;
          };
        })(name, prop[name]) :
        prop[name];
    }

    // The dummy class constructor
    function Class() {
        // Check to see if an object was passed to constructor.
        // If so, apply properties from passed in object to current this class' prototype.
        // This is handy for applying properties to a class by passing a settings object to the constructor.
        for (var i = 0; i < arguments.length; i++) {
            var arg = arguments[i];

            if(Object.isObject(arg)) {
                for (var key in arg) {
                    prototype[key] = arg[key];
                }
            }
        }

        // All construction is actually done in the init method
        if (!initializing && this.init)
            this.init.apply(this, arguments);
    }

    // Populate our constructed prototype object
    Class.prototype = prototype;

    // Enforce the constructor to be what we expect
    Class.prototype.constructor = Class;

    // And make this class extendable
    Class.extend = arguments.callee;

    Class.prototype.on = function(method, cb) {
        $(Class).on(method, cb);
    }

    Class.prototype.trigger = function(event) {
        var args = [].slice.apply(arguments);
        args.shift();
        $(Class).trigger(event, args);
    }

    return Class;
  };
})();

// A dump random data caching
App.cache = {};
App.temp = {};
App.user = {};

App.ajax = {
    post: function(url, data, cb) {
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            success: cb,
        })
    },
    _delete: function(url, data, cb) {
        $.ajax({
            url: url,
            type: 'DELETE',
            data: JSON.stringify(data),
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            success: cb,
        })
    },
    get: function(url, data, cb) {
        $.ajax({
            url: url,
            type: 'GET',
            data: data,
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            success: cb,
        })
    }
}
App.Views = {};

App.View = Class.extend({
    elm: null,
    events: {},

    _bindEvents: function() {
        for(var prop in this.events) {
            if (this.events.hasOwnProperty(prop)) {
                var sects = prop.split(' ');
                var eventName = sects[0];
                sects.shift();
                var selector = sects.join(' ');
                var callback = this[this.events[prop]] || function() {
                    console.log('CALLBACK DOES NOT EXIST')
                }
                $(this.elm).on(eventName, selector, callback);
            }
        }
    },

    init: function() {
        this.elm = $(this.elm)[0];
        var me = this;
        for(var key in this) {
            if (Object.isFunction(me[key])) me[key] = me[key].bind(this);
        }

        this._bindEvents()
    }
});
App.Models = {};
// Assumes REST API with convention based uri's
App.BaseModel = Class.extend({
    entity: '',
    fetch: function(cb) {
        var url;
        if(App.User) {
            url = '/api/' + this.entity + '/?access_token=' + App.User.token;
        } else {
            url = '/api/' + this.entity + '/';
        }
        $.get(url , cb)
    },
    fetchOne: function(id, cb) {
        var url;
        if(App.User) {
            url = '/api/' + this.entity + '/' + id + '?access_token=' + App.User.token;
        } else {
            url = '/api/' + this.entity + '/' + id;
        }
        $.get(url , cb)
    },
    post: function(options, cb) {
        var defaults = {
            url: '/api/' + this.entity + '/'
        }

        var settings = $.extend(defaults, options);

        AP.ajax.post(settings.url, settings.data, cb);
    }
});

$( document ).ajaxSuccess(function( event, xhr, settings, response ) {
    if(response && Object.isObject(response) && response.currentTab) {
        $('.navbar-nav .active').removeClass('active');
        $('.' + response.currentTab + 'Tab').addClass('active')
    }
});

App.Controllers = {};
App.showLoadingPage = function() {
    $(".page-inner").html("")
}
App.BaseController = Class.extend({
    routes: {},

    _showPage: function(template, model, sideNavModel) {
        model.AppConfig = AppConfig;
        model.req = {user: App.User};
        var content = $(App.templates[template](model)).hide()
        var page = $(".page-inner").html(content)
        content.fadeIn();
        //var page = $(".page-inner").html(App.templates[template](model))
        App.BindAjaxForms(page);
        FB.XFBML.parse(page[0]);
        
        App.scrollHandled = false;
        $(window).scrollTop(0)
        $('.navbar-collapse.in').collapse('hide')
        return page;
    },

    _bindRoutes: function() {
        for(var prop in this.routes) {
            if (this.routes.hasOwnProperty(prop)) {
                var route = prop;
                var handler= this[this.routes[prop]] || function() {
                    console.log('CALLBACK DOES NOT EXIST')
                }
                //this.route = crossroads.addRoute(route, handler)
    //              route.matched.add(console.log, console)
            }
        }
    },

    init: function() {
        var me = this;
        for(var key in this) {
            if (Object.isFunction(me[key])) me[key] = me[key].bind(this);
        }
        this._bindRoutes();
    }
})

App.BindAjaxForms = function(context) {
    $('[data-action=\"form\"]', context).ajaxForm({
        beforeSubmit: function(arr, form, options) {
            // TODO: This shouldn't be here, but ok.
            return form.triggerHandler('beforeSubmit', arguments);
        },
        success: function(response, statusText, xhr, form) {
            form.trigger('success', arguments)
        },
        error: function(xhr, statusText, code) {
            if(code == 'Forbidden') {
                return location.href = '/login'
            }
        }
    });
}

App.scrollHandled = false;

$(window).scroll(function () {
   if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {
      if(!App.scrollHandled){

        App.scrollHandled = $('[data-more-handler="true"]').triggerHandler('scrollBottom');

      }

   }
});
$(function() {
    $('body').on('click', '[data-type="navigator"]', function(e) {
        if(window.history && window.history.pushState && crossroads.navigator){
            e.preventDefault();
            crossroads.navigator.navigate($(this).attr('href'))
        } else {
            debugger;
        }
    })
    $("body").on("focus", "[data-show=\"datepicker\"]", function () {
        $(this).attr("autocomplete", "off");
        $(this).datepicker( {
            onSelect: function (dateText) {
                $(this).trigger('change', arguments);
            },
            beforeShowDay: $(this).data('validateDay'),
            onChangeMonthYear: $(this).data('onChangeMonthYear')
        });
        $(this).datepicker("show");
    });
    $('body').on('keyup', '[data-copyto]', function() {
        var context = $(this).closest('form');
        var element = $(this).data('copyto');
        $(element, context).val($(this).val() || $(this).text())
    })

    $('body').on('click', '.like-link', function(e) {
        $.getJSON($(this).attr('href'), function() {
            console.log(arguments)
        })
        e.preventDefault()
    })

    $('body').on('success', '.email-invite', function(e, response) {
      $('.invite-alert').removeClass('hide');
      $('.email-placeholder').html('&nbsp;' + $('.email-invite input').val());
      $('.email-invite input').val("").focus();
      setTimeout(function() {
        $('.invite-alert').addClass('hide')
      }, 4000)
    });


    App.BindAjaxForms($('body'))

    var showtimePicker =  function () {
        var $this = $(this);
        $this.timePicker({
            show24Hours: false,
            separator: ':',
        })

        // Get the Hours and set them before we show the picker.
        var hours = $this.triggerHandler('show');

        if(hours){
            $this.data('times', hours)
        } else {
            console.log($this)
            $this.removeData("times")
            console.log($this.data('times'))
        }
        $this.trigger("click");
    };

    $("body").on("focus", "[name=\"StartTime\"]", showtimePicker);
    $("body").on("focus", "[data-action=\"timePicker\"]", showtimePicker);
});

$(function() {
  $('body').on('click', '.toggleLink', function(e) {
    $($(this).attr('href')).toggleClass('hide');
    if($('.glyphicon-chevron-down', this).length)
        $('.glyphicon-chevron-down', this).addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
    else
        $('.glyphicon-chevron-up', this).addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up')
    e.preventDefault()
  });

  $('body').on('click', '.tabLink', function(e) {
    $($(this).attr('href')).removeClass('hide').siblings('.tab-pane').addClass('hide');
    e.preventDefault()
    $(this).addClass('hide').siblings('.tabLink').removeClass('hide')
  });

  $('.ajaxLink').click(function(e) {
    App.ajax.get($(this).attr('href'))
    e.preventDefault()
  });
})

$(function() {
    $('body').on('keydown keypress input', 'div[data-placeholder]', function() {
        if (this.textContent) {
            this.dataset.divPlaceholderContent = 'true';
        }
        else {
            delete(this.dataset.divPlaceholderContent);
        }
    });



    $('[data-behavior="autocomplete"]').each(function() {
        var ac = new App.Views.AutoComplete({elm: this});
    })

    $('body').on('submit', '.navbar-form', function(e) {
        e.preventDefault();
        var query = $('[name="q"]', this).val();
        crossroads.navigator.navigate('/search?q=' + query);
        return false;

    })
    $('[data-tabber="true"]').click(function(e) {
        var tab = $($(this).attr('href'));
        $('[data-tabber="true"]').removeClass('active')
        $(this).addClass('active');
        tab.addClass('active').siblings('.tab').removeClass('active')
        e.preventDefault();
    });

    
    $('body').on('click', '.header-back-btn', function(e) {
        e.preventDefault();
        crossroads.navigator.back();
    })
    if(App.User) {
        //var notifcationList = new App.Views.NotificationList();
        $('body').on('click', '.notificationsLink', function(e) {
            notifcationList.toggle()
            $('.notificationsLink .badge').addClass('hide')
            e.preventDefault();
            $(this).parent().toggleClass('active')
        })
    }

    $('body').on('click', '[data-share="facebook"]', function(e) {
        var me = this;
        var caption = $(this).closest('.verse').data('versetext');
        var text = $(this).closest('.verse').find('span.verse-text').text();
        var img = 'http://www.lastvine.com/images/verses/' + $(this).closest('.verse').data('verse') + '.png';
        debugger;
        FB.ui({
          method: 'feed',
          link: $(me).attr('href'),
          caption: caption,
          picture: img,
          description: text
        });
        e.preventDefault();
    })

    $('body').on('success', '.like-form', function(e, response) {
      $(this).children('button').remove();
      var likedButton = $('<a class="active btn btn-plain btn-sm" />').html('<span class="fa fa-heart" style="color:#9528AA"></span> Liked')
      $(this).append(likedButton)
    });

    $('body').on('success', '.favorite-form', function(e, response) {
      $(this).children('button').remove();
      var likedButton = $('<a class="active btn btn-plain btn-sm" />').html('<span class="fa fa-star" style="color:#9528AA"></span> Favorited')
      $(this).append(likedButton)
    });

    $('body').on('click', '.shareGroupToFacebook', function(e){
      FB.ui({
        method: 'send',
        link: $(e.target).attr('href'),
      });
      e.preventDefault()
    })
});
App.shareOnFb =  function(url) {
    window.open('http://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(url) + '&t=' + encodeURIComponent('aasdf'), 'Share on Facebook', "width=640,height=300");
}

if (!Date.prototype.toISOString) {
    Date.prototype.toISOString = function () {
        function pad(n) { return n < 10 ? '0' + n : n }
        return this.getUTCFullYear() + '-'
        + pad(this.getUTCMonth() + 1) + '-'
        + pad(this.getUTCDate()) + 'T'
        + pad(this.getUTCHours()) + ':'
        + pad(this.getUTCMinutes()) + ':'
        + pad(this.getUTCSeconds()) + 'Z';
    };
}
Date.prototype.toUniversalTime = function () {
    return new Date(this.getUTCFullYear(),
    this.getUTCMonth(),
    this.getUTCDate(),
    this.getUTCHours(),
    this.getUTCMinutes(),
    this.getUTCSeconds());
};


/**
 * Html5 Placeholder Polyfill - v2.0.9 - 2014-01-21
 * web: http://blog.ginader.de/dev/jquery/HTML5-placeholder-polyfill/
 * issues: https://github.com/ginader/HTML5-placeholder-polyfill/issues
 * Copyright (c) 2014 Dirk Ginader; Licensed MIT, GPL
 */

/*! http://mths.be/placeholder v2.0.7 by @mathias */
;(function(window, document, $) {

    // Opera Mini v7 doesn’t support placeholder although its DOM seems to indicate so
    var isOperaMini = Object.prototype.toString.call(window.operamini) == '[object OperaMini]';
    var isInputSupported = 'placeholder' in document.createElement('input') && !isOperaMini;
    var isTextareaSupported = 'placeholder' in document.createElement('textarea') && !isOperaMini;
    var prototype = $.fn;
    var valHooks = $.valHooks;
    var propHooks = $.propHooks;
    var hooks;
    var placeholder;

    if (isInputSupported && isTextareaSupported) {

        placeholder = prototype.placeholder = function() {
            return this;
        };

        placeholder.input = placeholder.textarea = true;

    } else {

        placeholder = prototype.placeholder = function() {
            var $this = this;
            $this
                .filter((isInputSupported ? 'textarea' : ':input') + '[placeholder]')
                .not('.placeholder')
                .bind({
                    'focus.placeholder': clearPlaceholder,
                    'blur.placeholder': setPlaceholder
                })
                .data('placeholder-enabled', true)
                .trigger('blur.placeholder');
            return $this;
        };

        placeholder.input = isInputSupported;
        placeholder.textarea = isTextareaSupported;

        hooks = {
            'get': function(element) {
                var $element = $(element);

                var $passwordInput = $element.data('placeholder-password');
                if ($passwordInput) {
                    return $passwordInput[0].value;
                }

                return $element.data('placeholder-enabled') && $element.hasClass('placeholder') ? '' : element.value;
            },
            'set': function(element, value) {
                var $element = $(element);

                var $passwordInput = $element.data('placeholder-password');
                if ($passwordInput) {
                    return $passwordInput[0].value = value;
                }

                if (!$element.data('placeholder-enabled')) {
                    return element.value = value;
                }
                if (value == '') {
                    element.value = value;
                    // Issue #56: Setting the placeholder causes problems if the element continues to have focus.
                    if (element != safeActiveElement()) {
                        // We can't use `triggerHandler` here because of dummy text/password inputs :(
                        setPlaceholder.call(element);
                    }
                } else if ($element.hasClass('placeholder')) {
                    clearPlaceholder.call(element, true, value) || (element.value = value);
                } else {
                    element.value = value;
                }
                // `set` can not return `undefined`; see http://jsapi.info/jquery/1.7.1/val#L2363
                return $element;
            }
        };

        if (!isInputSupported) {
            valHooks.input = hooks;
            propHooks.value = hooks;
        }
        if (!isTextareaSupported) {
            valHooks.textarea = hooks;
            propHooks.value = hooks;
        }

        $(function() {
            // Look for forms
            $(document).delegate('form', 'submit.placeholder', function() {
                // Clear the placeholder values so they don't get submitted
                var $inputs = $('.placeholder', this).each(clearPlaceholder);
                setTimeout(function() {
                    $inputs.each(setPlaceholder);
                }, 10);
            });
        });

        // Clear placeholder values upon page reload
        $(window).bind('beforeunload.placeholder', function() {
            $('.placeholder').each(function() {
                this.value = '';
            });
        });

    }

    function args(elem) {
        // Return an object of element attributes
        var newAttrs = {};
        var rinlinejQuery = /^jQuery\d+$/;
        $.each(elem.attributes, function(i, attr) {
            if (attr.specified && !rinlinejQuery.test(attr.name)) {
                newAttrs[attr.name] = attr.value;
            }
        });
        return newAttrs;
    }

    function clearPlaceholder(event, value) {
        var input = this;
        var $input = $(input);
        if (input.value == $input.attr('placeholder') && $input.hasClass('placeholder')) {
            if ($input.data('placeholder-password')) {
                $input = $input.hide().next().show().attr('id', $input.removeAttr('id').data('placeholder-id'));
                // If `clearPlaceholder` was called from `$.valHooks.input.set`
                if (event === true) {
                    return $input[0].value = value;
                }
                $input.focus();
            } else {
                input.value = '';
                $input.removeClass('placeholder');
                input == safeActiveElement() && input.select();
            }
        }
    }

    function setPlaceholder() {
        var $replacement;
        var input = this;
        var $input = $(input);
        var id = this.id;
        if (input.value == '') {
            if (input.type == 'password') {
                if (!$input.data('placeholder-textinput')) {
                    try {
                        $replacement = $input.clone().attr({ 'type': 'text' });
                    } catch(e) {
                        $replacement = $('<input>').attr($.extend(args(this), { 'type': 'text' }));
                    }
                    $replacement
                        .removeAttr('name')
                        .data({
                            'placeholder-password': $input,
                            'placeholder-id': id
                        })
                        .bind('focus.placeholder', clearPlaceholder);
                    $input
                        .data({
                            'placeholder-textinput': $replacement,
                            'placeholder-id': id
                        })
                        .before($replacement);
                }
                $input = $input.removeAttr('id').hide().prev().attr('id', id).show();
                // Note: `$input[0] != input` now!
            }
            $input.addClass('placeholder');
            $input[0].value = $input.attr('placeholder');
        } else {
            $input.removeClass('placeholder');
        }
    }

    function safeActiveElement() {
        // Avoid IE9 `document.activeElement` of death
        // https://github.com/mathiasbynens/jquery-placeholder/pull/99
        try {
            return document.activeElement;
        } catch (err) {}
    }

}(this, document, jQuery));

$(function() {
    $('input, textarea').placeholder();
})
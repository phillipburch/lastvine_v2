var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  searchService = require('../service/search'),
  request = require('request');

var controller = {

  search: function(req, res) {
    var range = bibleService.findRange(query),
      options = {
        query: {
          multi_match: {
            query: query,
            fields: ['name', 'text', 'title']
          }
        },
        index: 'schejule',
        size: 100,
        highlight : {
          fragment_size : 400,
          fields : {name: {}, text: {} }
        }
      };

    searchService.searchAll(options, function(err, result) {
      if(result.verses) {
        result.verses = result.verses.each(function(item) {
          item.readableName = bibleService.getReadableNameFromKey(item._source.verseKey);
        });
      }
      return respond(res, 'search/searchResults', result);
    });
  }
};

module.exports = controller;
/*
 * GET users listing.
 */

var mongoose = require('mongoose'),
  redis = require('redis'),
  uuid = require('node-uuid'),
  Notification = mongoose.model('Notification'),
  elasticSearchClient = require('elasticsearchclient'),
  User = mongoose.model('User');

var controller = {
  getNotifications: function (req, res) {
    var client = redis.createClient();
    client.on('connect', function() {
      var key = 'notification:user:' + req.user._id;
      client.zrevrange(key, 0, 10, function(err, notificationIds) {
        Notification.find({_id : {'$in': notificationIds}})
          .populate('actor', 'name')
          .sort('-createdOn')
            .exec(function(err, notifications) {
              notifications = notifications.filter(function(n) {
                return n.createdOn.isAfter(Date.create().addDays(-3));
              });
              req.user.unreadNotificationCount = 0;
              req.user.save();
              res.json({notifications: notifications});
            });
      });
    });
  },

  count: function(req, res) {
    var client = redis.createClient();
    client.on('connect', function() {
      res.json({count: req.user.unreadNotificationCount});
    });
  }
};

module.exports = controller;
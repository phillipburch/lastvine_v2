var redis = require('redis'),
  when = require('when'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  ImageAsset = mongoose.model('ImageAsset'),
  geoip = require('geoip-lite');


exports.home = function(test, help){
  if(!this.user)
    return when({
      model: {},
      template: 'loggedoutindex'
    });
  else
    return when({
      model: {user: this.user},
      template: 'index'
    });
};

exports.howitworks = function(req, res){
  return when({
    model: {},
    template: 'howitworks'
  });
};

exports.getHeader = function(req, res) {
};

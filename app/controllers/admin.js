/*
 * GET users listing.
 */

var mongoose = require('mongoose'),
  redis = require('redis'),
  uuid = require('node-uuid'),
  imageService = require('../service/image'),
  User = mongoose.model('User');

var controller = {
  settings: function(req, res) {
    if(!req.user)
      return res.render('500');

    res.render('user/show', {
      item: req.user,
      currentTab: 'me'
    });
  }
};

module.exports = controller;
var redis = require('redis');

var Sockets = {
  newMessage: function(curUser, data, socket) {
    data.curUser = curUser;
    data.timestamp = Date.create();
    socket.broadcast.emit('messageDelivered', data);
    socket.emit('messageDelivered', data);

    var client = redis.createClient();
    client.on('connect', function () {
      var redisArgs = ['chatroom', data.timestamp.getTime(), JSON.stringify(data)];
      client.zadd(redisArgs, function(err, response) {
        console.log(arguments);
      });
    });
  },

  typingStarted: function(curUser, data, socket) {
    socket.broadcast.emit('typingStarted', curUser);
  },

  typingStopped: function(curUser, data, socket) {
    socket.broadcast.emit('typingStopped', curUser);
  },

  init: function() {
    var me = this;
    io.sockets.on('connection', function (socket) {
      var user = socket.handshake.user;

      socket.on('newMessage', function (data) {
        me.newMessage(user, data, socket);
      });

      socket.on('typingStarted', function (data) {
        me.typingStarted(user, data, socket);
      });

      socket.on('typingStopped', function (data) {
        me.typingStopped(user, data, socket);
      });
    });
  }
};

return Sockets;

/*
 * GET users listing.
 */

var mongoose = require('mongoose')
  , redis = require('redis')
  , w = require('when')
  , nodefn = require('when/node')
  , uuid = require('node-uuid')
  , service = require('../service')
  , elasticSearchClient = require('elasticsearchclient')
  , validator = require('validator')
  , User = mongoose.model('User');

var controller = {

  unsubscribe: function() {
    return w({template: 'user/unsubscribe'});
  },

  forgotPassword: function() {
    return w({template: 'user/forgotPassword'});
  },

  recoverPassword: function(token) {
    var client = redis.createClient(),
      key ='forgotpassword:' + token;

    return nodefn.call(client.get.bind(client), key)
      .then(function(email) {
        if(!email)
          return {redirect: '/'};

        return {template: 'user/recoverpassword', model: {email: email, token: token}};
      }, function(err) {
        return err;
      });
  },

  edit: function(id) {
    if(!this.user || (this.user._id !== id))
      return w.reject({errorCode: 403, message: 'Not Authorized'});

    return User.findOne({_id: id}).exec()
      .then(function(user) {
        return {template: 'user/edit', model: {item: user}};
      }, function(error) {
        return {template: 'user/edit', model: error};
      });
  },

  show: function(id) {
    var client = redis.createClient();
    var multi = client.multi();
    var followerCount, followingCount, me, isFollowee;

    return service.user.getUser(id).then(function(user) {
      return{
        template: 'user/show',
        model: {
          item: user,
          posts: [],
          tab: 'posts'
        }
      };
    }, function(err) {
      return {
        error: err
      };
    });
  },

  settings: function() {
    var self = this;
    if(!this.user)
      return w.reject({errorCode: 401, message: 'You have to be logged in'});

    return w({template: 'user/show', model: {
      item: self.user
    }});
  }
};

module.exports = controller;
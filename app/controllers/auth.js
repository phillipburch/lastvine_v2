
/*
 * GET users listing.
 */

var mongoose = require('mongoose'),
  redis = require('redis'),
  w = require('when'),
  imageService = require('../service/image'),
  emailService = require('../service/email'),
  User = mongoose.model('User');

var controller = {
  signin: function (next) {
    return w({template: 'auth/signin', model: {
      title: 'Sign in',
      user: new User(),
      next: next,
      navDisabled: true
    }});
  },

  // login
  login: function (next) {
    return w({redirect: next || '/'});
  },

  // logout
  logout: function () {
    this.req.logout();
    return w({redirect: '/'});
  },

  // session
  session: function (req, res) {
    return w({redirect: '/'});
  },

  signup: function(token, next) {
    if(this.user)
      return w({redirect: '/'});

    return w({template: 'auth/signup', model: {
      title: 'Sign up',
      user: new User(),
      next: next,
      //group: group,
      token: token
    }});
  },


  signup2: function (req, res) {
    return w({template: 'user/signup2'});
  },

  forgotPassword: function(email) {
    return w({template: '', model: {email: email}});
  }
};

module.exports = controller;
var _ = require('underscore'),
  when = require('when'),
  decorate;

function extractArgumentMap(functionCode) {
  var argumentStringMatch = functionCode.match(new RegExp('\\([^)]*\\)', ''));

  var argumentMap = argumentStringMatch[0].match(new RegExp('[^\\s,()]+', 'g'));

  if(!argumentMap || !argumentMap.length)
    return([]);

  return(argumentMap);
}

function invoke (method, namedArguments, context) {
  var orderedArguments = [];

  method.argumentMap = extractArgumentMap(method.toString());

  for (var i = 0 ; i < method.argumentMap.length ; i++){
    if (method.argumentMap[ i ] in namedArguments){
      orderedArguments.push(namedArguments[method.argumentMap[i]]);
    } else {
      orderedArguments.push(null);
    }
  }

  return method.apply(context, orderedArguments);
}

decorate = function (method) {
  return function (req, res) {
    var options = _.extend(req.body, req.files, req.query, req.params),
      context = {
        req: req,
        res: res,
        user: req.user
      };

    invoke(method, options, context).then(function (result) {
      if(res.locals.isJson) {
        if(result.redirect)
          return res.json(result);

        res.json(result.model);
      }  else {
        if(result.redirect)
          return res.redirect(result.redirect);

        res.render(result.template, result.model);
      }
    }, function (error) {
      console.log(error);
      var errorCode = error.errorCode || 500;
      res.json(errorCode, error);
    });
  };
};


module.exports = {
  auth: require('./auth'),
  home: require('./home'),
  sitemap: require('./sitemap'),
  social: require('./social'),
  search: require('./search'),
  imageAsset: require('./imageasset'),
  user: require('./user'),
  decorate: decorate
};
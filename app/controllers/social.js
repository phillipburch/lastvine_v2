/*
 * GET home page.
 */
var mongoose = require('mongoose'),
  request = require("request"),
  geoip = require('geoip-lite');

exports.facebookRedirects = function(req, res){
  if (!req.user) {
    res.redirect('/facebook/step2');
  } else {
    res.redirect('/');
  }
};

exports.getFBFriends = function (req, res) {
  request('https://graph.facebook.com/me/friends?access_token=' +req.user.facebookToken , function (error, response, body) {
    if (!error && response.statusCode == 200) {
      res.json(JSON.parse(body).data);
      //console.log(body) // Print the google web page.
    }
  });
};

exports.step2 = function(req, res) {
  res.render('social/step2', {
    user: req.session.fbProfile
  });
};

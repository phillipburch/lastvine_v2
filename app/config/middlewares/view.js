function insertAt(text, index, newText) {
    var inBrackets = false, chars = text.split(''), count = 0;
    for(var i = 0; i <= chars.length; i++) {
      var c = chars[i];
      if(c == '<') {
        inBrackets = true;
      }
      if(!inBrackets) {
        if(index == count) {
          return chars.insert(newText.split(''), i).join('');
        }
        count++;
      }
      if(c == '>') {
        inBrackets = false;
      }
    }
}
module.exports = function (config) {
  return function (req, res, next) {
    res.locals.appName = config.app.name;
    res.locals.title = config.app.name;

    res.locals.currentTab = res.currentTab;
    res.locals.req = req;

    res.locals.isActive = function (link) {
      return req.url === link ? 'active' : '';
    };

    next();
  };
};
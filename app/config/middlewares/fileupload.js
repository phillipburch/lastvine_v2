//FileUpload Controller has no access to session and is simply a middleware
var formidable = require('formidable'),
  imageService = require('../../service/image'),
  uuid = require('node-uuid'),
  mongoose = require('mongoose'),
  imageAsset = mongoose.model('ImageAsset');

FileUpload =  {
  handleUploads: function(req, res, next) {
    var socket, files, imageId = uuid.v4();

    if (req.url.indexOf('/upload') == -1)
      return next();

    res.json({success: true, assetId: imageId});

    var form = new formidable.IncomingForm(req);

    form.on('field', function(name, value) {
      if(name== "socketId"){
        socket = io.sockets.socket(value);
      }
    });

    form.uploadDir =  AppConfig.root + '/temp';
    form.on('progress', function(bytesReceived, bytesExpected) {
      console.log(((bytesReceived / bytesExpected)*100) + "% uploaded");
      if(socket)
        socket.emit('uploadProgress', ((bytesReceived / bytesExpected)*100) + "% uploaded");
    });

    form.parse(req, function(err, fields, files) {
      imageService.save(Date.create().format("{yyyy}{mm}{dd}"), imageId, 800, null, files.mainImage, function(url) {
        var asset = new imageAsset({_id: imageId, url: url});
          asset.save(function() {
            socket.emit('uploadFinished', asset);
          });
      });
    });
  },

  initSocket: function(socket) {
    socket.on('registerUpload', function() {
      socket.emit('socketId', socket.id);
    });
  }
};

onIOInitiated(function() {
  io.sockets.on('connection', function (socket) {
    FileUpload.initSocket(socket);
  });
});

module.exports = FileUpload;
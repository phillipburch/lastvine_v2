
var signature = require('cookie-signature');
var RedisStore = require('socket.io/lib/stores/redis')
  , redis  = require("redis")
  , pub    = redis.createClient()
  , sub    = redis.createClient()
  , client = redis.createClient();

var parseSignedCookies = function(obj, secret){
  var ret = {};
  Object.keys(obj).forEach(function(key){
    var val = obj[key];
    if (0 == val.indexOf('s:')) {
      val = signature.unsign(val.slice(2), secret);
      if (val) {
        ret[key] = val;
        delete obj[key];
      }
    }
  });
  return ret;
};

parseJSONCookie = function(str) {
  if (0 == str.indexOf('j:')) {
    try {
      return JSON.parse(str.slice(2));
    } catch (err) {
      // no op
    }
  }
};

parseJSONCookies = function(obj){
  Object.keys(obj).forEach(function(key){
    var val = obj[key];
    var res = parseJSONCookie(val);
    if (res) obj[key] = res;
  });
  return obj;
};

/**
 * Parse a signed cookie string, return the decoded value
 *
 * @param {String} str signed cookie string
 * @param {String} secret
 * @return {String} decoded value
 * @api private
 */

var parseSignedCookie = function(str, secret){
  return 0 == str.indexOf('s:')
    ? signature.unsign(str.slice(2), secret)
    : str;
};

module.exports = function (server, sessionStore, cookieParser, passport) {
  var io = require('socket.io');
  var io = io.listen(server)
    , cookie = require('cookie');

  io.configure(function (){
    io.set('log level', 1);
    io.set('store', new RedisStore({
      redisPub : pub
    , redisSub : sub
    , redisClient : client
    }));
    io.set('authorization', function (handshakeData, callback) {
      var cookies = handshakeData.headers.cookie;

      secret = 'noobjs';
      mycookies = {};
      signedCookies = {};

      if (cookies) {
        try {
          mycookies = cookie.parse(cookies);
          if (secret) {
            signedCookies = parseSignedCookies(mycookies, secret);
            signedCookies = parseJSONCookies(signedCookies);
          }
          mycookies = parseJSONCookies(mycookies);
        } catch (err) {
          return callback(null, false);
        }
      }

      sessionStore.load(signedCookies['connect.sid'], function (storeErr, session) {
          passport.deserializeUser(session.passport.user, function(err, user) {
            handshakeData.user = user;
          callback(null, true);
          })
        });
    });
  });

  return io;
}
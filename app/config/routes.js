var api = require('../controllers');

module.exports = function (app, passport, auth) {
  //Auth
  app.get('(/api)?', api.decorate(api.home.home));
  app.get('(/api)?/login', api.decorate(api.auth.signin));
  app.post('(/api)?/user/create', api.decorate(api.auth.create));
  app.post('(/api)?/user/signup', api.decorate(api.auth.create));
  app.get('(/api)?/signup', api.decorate(api.auth.signup));
  app.get('(/api)?/user/signup2', api.decorate(api.auth.signup2));
  app.post('/api/phone/signup', api.decorate(api.auth.phoneCreate));
  app.post('(/api)?/user/login',
    passport.authenticate('local', {
      failureRedirect: '/login'
    }), api.decorate(api.auth.login));
  app.get('(/api)?/user/logout', api.decorate(api.auth.logout));

  // User
  app.get('(/api)?/user', api.decorate(api.user.list));
  app.get('(/api)?/user/settings', api.decorate(api.user.settings));
  app.post('(/api)?/user/image', api.user.uploadImage);

  app.get('/user/:id', api.decorate(api.user.show));
  app.get('/api/user/:id', api.decorate(api.user.show));

  app.get('/user/:id/edit', api.decorate(api.user.edit));
  app.get('(/api)?/unsubscribe', api.decorate(api.user.unsubscribe));
  app.post('(/api)?/unsubscribe', api.decorate(api.user.saveUnsubscribe));

  // Uploads
  app.post('/api/asset/upload', api.imageAsset.upload);

  //Search
  app.get('(/api)?/search', api.search.search);

  // Social
  app.get('/verify/facebook', passport.authenticate('facebook'));
  app.get('/facebook', api.social.facebookRedirects );
  app.get('/facebook/getFriends', api.social.getFBFriends );
  app.get('/facebook/step2', api.social.step2 );
  app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { successRedirect: '/verify/user/?facebookSuccess',
                                        failureRedirect: '/login' }));
  app.get('/auth/twitter/callback',
    passport.authenticate('twitter', { successRedirect: '/?twittersuccess=1',
                                       failureRedirect: '/login' }));

  app.get('/sitemap.xml', api.sitemap.index );
};
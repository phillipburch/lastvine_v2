var mongoose = require('mongoose')
  , LocalStrategy = require('passport-local').Strategy
  , FacebookStrategy = require('passport-facebook').Strategy
  , TwitterStrategy = require('passport-twitter').Strategy
  , BearerStrategy = require('passport-http-bearer').Strategy
  , elasticSearchClient = require('elasticsearchclient')
  , Config = require('./config')
  , User = mongoose.model('User')


module.exports = function (passport, config) {
  // require('./initializer')
  // serialize sessions
  passport.serializeUser(function(user, done) {
    done(null, user.id)
  })

  passport.deserializeUser(function(id, done) {
    User.findOne({ _id: id }, function (err, user) {
      done(err, user)
    })
  })

passport.use(new BearerStrategy(
  function(token, done) {
    User.findOne({ token: token }, function (err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      return done(null, user, { scope: 'all' });
    });
  }
));

  // use local strategy
  passport.use(new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password'
    },
    function(email, password, done) {
      User.findOne({ email: email.toLowerCase() }, function (err, user) {
        if (err) { return done(err) }
        if (!user) {
          return done(null, false, { message: 'Unknown user' })
        }
        if (!user.authenticate(password)) {
          return done(null, false, { message: 'Invalid password' })
        }
        return done(null, user)
      })
    }
  ))

  passport.use(new FacebookStrategy({
    clientID: AppConfig.facebook.clientID,
    clientSecret: AppConfig.facebook.clientSecret,
    callbackURL: AppConfig.facebook.callbackURL,
    passReqToCallback: true
    },
    function(req, accessToken, refreshToken, profile, done) {
      User.findOne({facebookId: profile.id}).exec(function(err, item) {
        if(item) {
          item.facebookId = profile.id;
          item.facebookToken = accessToken;
          item.provider = 'facebook';
          item.save(function() {
            done(null, item);
          })
        } else {
          //console.log(profile)
          var user = new User({
            name: profile.displayName,
            facebookId: profile.id,
            email: profile.emails[0].value,
            facebookToken: accessToken,
            provider: 'facebook'
          });

          user.save(function(err, item) {
            done(null, item);
           //Index the USER
            var searchClient = new elasticSearchClient(AppConfig.elasticSearch);
            searchClient.index('lastvine', 'user', item, item._id.toString()).on('data', function(data) {
                console.log(data)
            }).exec()
          });
        }
      })
    }
  ));

  passport.use(new TwitterStrategy({
      consumerKey: AppConfig.twitter.consumerKey,
      consumerSecret: AppConfig.twitter.consumerSecret,
      callbackURL: AppConfig.twitter.callbackURL,
      passReqToCallback: true
    },
    function(req, accessToken, refreshToken, profile, done) {
      // This needs to find or create a real user. Sorta like facebook
      User.findOne({twitterId: profile.id}).exec(function(err, item) {
        if(item) {
          console.log(item)
          done(null, item)
        } else {
          var user = new User({
            images: profile.photos,
            name: profile.displayName,
            twitterId: profile.id,
            twitterHandle: profile.username,
            location: profile._json.location,
            backgroundImage: profile._json.profile_background_image_url
          });
          user.save(function(err, item) {
            done(null, item);
          });
        }
      })
    }
  ));
};
App.Views.Search = App.View.extend({

  init: function () {
    this._super();
    var me = this;
    $('.btn-link').click(function(e){
      App.shareOnFb($(this).data('url'))
    })
    $('.navbar input[name="q"]').val(this.title)
    $('.moreLink').each(function() {
      $(this).popover({
        html: true,
        content: '&nbsp;'
      });
    }).on('shown.bs.popover', function(e) {
      var template = ($(this).data('target') == 'related') ? '_newRelatedVerse' : '_newTags';

      $(this).data('bs.popover')
        .$tip.find(".popover-content")
        .html(App.templates[template]({
          verses: [],
          osisID: $(this).data('osisid')
        }));
      $(this).data('bs.popover').updatePlacement()
      App.BindAjaxForms($(this).data('bs.popover').$tip)
    }).click(function(e) {e.preventDefault()})
  }
})
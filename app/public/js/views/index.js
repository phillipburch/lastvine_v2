App.Views.Index = App.View.extend({
  events: {
    'click .verse-favorite': 'favorite',
    'click .verse-comment': 'comment',
    'click .verse-question': 'question',
    'click .verse-share': 'share'
  },

  favorite: function(e) {
    var verse = $(e.target).closest('.verse').data('verse');
    App.ajax.get('/bible/' + verse + '/like', {passageId: this.passageId }, function() {

    })
    e.preventDefault();
  },

  comment: function(e) {
    var popup = new App.Views.Popup({
      target: $(e.target).closest('.list-group-item'),
      postType: "comment"
    });
    e.preventDefault();
  },

  question: function(e) {
    var popup = new App.Views.Popup({
      target: $(e.target).closest('.list-group-item'),
      postType: "question"
    });
    e.preventDefault();
  },

  share: function(e) {
    App.shareOnFb('http://www.lastvine.com')
  },

  init: function () {
    this._super();
    this.passageId = $('.home-feed').data('passage');
  }
});

App.Views.Popup = App.View.extend({
  events: {
    'click .closePost': 'destroy'
  },

  target: null,

  destroy: function(e) {
    $(this.elm).remove();
    $(this.target).siblings().fadeTo('slow', 1);
    e.preventDefault();
  },

  init: function () {
    var req = {user: App.User};
    $('.newAnnotateHolder').remove();

    var verse = $(this.target).children('.verse').data('verse');
    var popup =  $(App.templates._smallnewquestion({
      req: req,
      passage: verse,
      postType: this.postType,
      collapsed: false,
      disableTitle: true,
      currentPassageId: $('.home-feed').data('passage')
    }));

    popup.removeClass('hide').data('popover', this);

    $(this.target).append(popup).siblings().fadeTo('slow', .3);


    $('body,html').animate({scrollTop: $(this.target).offset().top}, 200)

    this.elm = popup;
    App.BindAjaxForms();
    this._super();
  }
})
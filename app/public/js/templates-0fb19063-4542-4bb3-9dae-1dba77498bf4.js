this["App"] = this["App"] || {};
this["App"]["templates"] = this["App"]["templates"] || {};

this["App"]["templates"]["_comment"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),c = locals_.c;buf.push("<li class=\"media\"><a" + (jade.attrs({ 'href':('/user/' + (c.user._id) + ''), "class": [('pull-left')] }, {"href":true})) + ">");
if ((c.user.thumbnailUrl))
{
buf.push("<img" + (jade.attrs({ 'src':('' + (c.user.thumbnailUrl) + ''), "class": [('media-object')] }, {"src":true})) + "/>");
}
else
{
buf.push("<img src=\"/img/grey-silhouette.png\" style=\"height:30px;\" class=\"media-object\"/>");
}
buf.push("</a><div class=\"media-body\"><div class=\"media-heading\"><a" + (jade.attrs({ 'href':('/user/' + (c.user._id) + '') }, {"href":true})) + ">" + (jade.escape((jade.interp = c.user.name) == null ? '' : jade.interp)) + "</a></div><div>" + (jade.escape((jade.interp = c.text) == null ? '' : jade.interp)) + "</div><div style=\"text-align:right\"><small>" + (jade.escape((jade.interp = Date.create(c.createDt).relative()) == null ? '' : jade.interp)) + "</small></div></div></li>");;return buf.join("");
};

this["App"]["templates"]["_feedList"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),posts = locals_.posts,req = locals_.req;buf.push("<div>");
// iterate posts
;(function(){
  var $$obj = posts;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var p = $$obj[$index];

if ( p.type == "note")
{
buf.push("<li class=\"clearfix\"><div class=\"item-content-holder media-body note-list-body\"><h4 style=\"margin:0\" class=\"clearfix\"><a" + (jade.attrs({ 'href':("/user/" + (p.user._id) + ""), 'data-type':('navigator'), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':("" + (p.user.thumbnailUrl) + ""), 'width':("30"), "class": [('img-circle'),('pull-right')] }, {"src":true,"width":true})) + "/></a><a" + (jade.attrs({ 'href':("/user/" + (p.user._id) + ""), 'data-type':('navigator'), "class": [('normal-link')] }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = p.user.name) == null ? '' : jade.interp)) + "</a>");
if ((p.topicVerses && p.topicVerses.length))
{
buf.push("<small>&nbsp;commented on&nbsp;</small><a" + (jade.attrs({ 'href':("/verse/" + (p.topicVerses[0].osisID) + ""), 'data-type':('navigator'), "class": [('normal-link')] }, {"href":true,"data-type":true})) + "><strong>" + (jade.escape((jade.interp = p.topicVerses[0].readableName) == null ? '' : jade.interp)) + "</strong></a>");
}
buf.push("</h4><div class=\"post-snippet note-body-text\">" + (((jade.interp = p.text) == null ? '' : jade.interp)) + "</div>");
if ((p.topicVerses && p.topicVerses.length))
{
buf.push("<blockquote><p class=\"text-muted\">" + (jade.escape((jade.interp = p.topicVerses[0].text) == null ? '' : jade.interp)) + "</p></blockquote>");
}
if ((p.images))
{
buf.push("<div class=\"post-images\">");
// iterate p.images
;(function(){
  var $$obj = p.images;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var img = $$obj[$index];

if ((img.url))
{
buf.push("<img" + (jade.attrs({ 'src':("" + (img.url) + "") }, {"src":true})) + "/>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var img = $$obj[$index];

if ((img.url))
{
buf.push("<img" + (jade.attrs({ 'src':("" + (img.url) + "") }, {"src":true})) + "/>");
}
    }

  }
}).call(this);

buf.push("</div>");
}
buf.push("<div class=\"note-footer clearfix\"><div class=\"pull-right\"><small class=\"text-muted\">" + (jade.escape((jade.interp = Date.create(p.createdOn).relative()) == null ? '' : jade.interp)) + "</small></div><div class=\"post-actions clearfix\"><ul class=\"list-inline action-list\"><li><form" + (jade.attrs({ 'method':("GET"), 'action':("/api/note/" + (p._id) + "/like"), 'data-action':("form") }, {"method":true,"action":true,"data-action":true})) + "><button type=\"submit\" class=\"muted btn btn-plain btn-sm\"><span class=\"fa fa-heart-o\"></span>&nbsp; Like</button></form></li><li><a" + (jade.attrs({ 'href':("#comment_" + (p._id) + ""), "class": [('muted'),('toggleLink'),('btn'),('btn-plain'),('btn-sm')] }, {"href":true})) + "><span class=\"fa fa-reply\"></span>&nbsp; Reply</a></li></ul></div></div><div" + (jade.attrs({ 'id':("comment_" + (p._id) + ""), "class": [('note-comments'),("" + (p.comments.length > 0 ? 'show' : 'hide') + ""),('hide')] }, {"id":true})) + ">");
if ((p.likes.length))
{
buf.push("<div><a href=\"#\"><small><span class=\"fa fa-heart\"></span>&nbsp; " + (jade.escape((jade.interp = p.likes.length) == null ? '' : jade.interp)) + " Likes</small></a></div>");
}
// iterate p.comments
;(function(){
  var $$obj = p.comments;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var c = $$obj[$index];

buf.push("<div class=\"media clearfix\"><a href=\"#\" class=\"pull-left\"><img" + (jade.attrs({ 'src':("" + (c.user.thumbnailUrl) + ""), "class": [('media-object'),('img-circle')] }, {"src":true})) + "/></a><div class=\"media-body\"><strong>" + (jade.escape((jade.interp = c.user.name) == null ? '' : jade.interp)) + "</strong><p class=\"comment-list-body\">" + (jade.escape((jade.interp = c.text) == null ? '' : jade.interp)) + "</p><small style=\"color:#999\" class=\"muted pull-right\">" + (jade.escape((jade.interp = Date.create(c.createDt).relative()) == null ? '' : jade.interp)) + "</small></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var c = $$obj[$index];

buf.push("<div class=\"media clearfix\"><a href=\"#\" class=\"pull-left\"><img" + (jade.attrs({ 'src':("" + (c.user.thumbnailUrl) + ""), "class": [('media-object'),('img-circle')] }, {"src":true})) + "/></a><div class=\"media-body\"><strong>" + (jade.escape((jade.interp = c.user.name) == null ? '' : jade.interp)) + "</strong><p class=\"comment-list-body\">" + (jade.escape((jade.interp = c.text) == null ? '' : jade.interp)) + "</p><small style=\"color:#999\" class=\"muted pull-right\">" + (jade.escape((jade.interp = Date.create(c.createDt).relative()) == null ? '' : jade.interp)) + "</small></div></div>");
    }

  }
}).call(this);

buf.push("<form" + (jade.attrs({ 'method':("post"), 'action':("/note/" + (p._id) + "/comment"), 'data-action':("form"), "class": [('comment')] }, {"method":true,"action":true,"data-action":true})) + "><input" + (jade.attrs({ 'type':("hidden"), 'name':("user"), 'value':("" + (req.user._id) + "") }, {"type":true,"name":true,"value":true})) + "/><div contenteditable=\"true\" data-placeholder=\"Add a comment...\" data-copyto=\"[name='text']\" class=\"textarea form-control\"></div><input type=\"hidden\" name=\"text\"/><div class=\"clearfix footer\"><button type=\"submit\" class=\"btn btn-primary btn-sm pull-right\">Save</button></div></form></div></div><hr/></li>");
}
else if ( p.type == 'joined')
{
buf.push("<li class=\"clearfix\"><div class=\"item-content-holder media-body note-list-body\"><h5><a" + (jade.attrs({ 'href':("/user/" + (p.user._id) + "") }, {"href":true})) + ">" + (jade.escape((jade.interp = p.user.name) == null ? '' : jade.interp)) + "</a>&nbsp;joined the group.</h5></div><hr/></li>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var p = $$obj[$index];

if ( p.type == "note")
{
buf.push("<li class=\"clearfix\"><div class=\"item-content-holder media-body note-list-body\"><h4 style=\"margin:0\" class=\"clearfix\"><a" + (jade.attrs({ 'href':("/user/" + (p.user._id) + ""), 'data-type':('navigator'), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':("" + (p.user.thumbnailUrl) + ""), 'width':("30"), "class": [('img-circle'),('pull-right')] }, {"src":true,"width":true})) + "/></a><a" + (jade.attrs({ 'href':("/user/" + (p.user._id) + ""), 'data-type':('navigator'), "class": [('normal-link')] }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = p.user.name) == null ? '' : jade.interp)) + "</a>");
if ((p.topicVerses && p.topicVerses.length))
{
buf.push("<small>&nbsp;commented on&nbsp;</small><a" + (jade.attrs({ 'href':("/verse/" + (p.topicVerses[0].osisID) + ""), 'data-type':('navigator'), "class": [('normal-link')] }, {"href":true,"data-type":true})) + "><strong>" + (jade.escape((jade.interp = p.topicVerses[0].readableName) == null ? '' : jade.interp)) + "</strong></a>");
}
buf.push("</h4><div class=\"post-snippet note-body-text\">" + (((jade.interp = p.text) == null ? '' : jade.interp)) + "</div>");
if ((p.topicVerses && p.topicVerses.length))
{
buf.push("<blockquote><p class=\"text-muted\">" + (jade.escape((jade.interp = p.topicVerses[0].text) == null ? '' : jade.interp)) + "</p></blockquote>");
}
if ((p.images))
{
buf.push("<div class=\"post-images\">");
// iterate p.images
;(function(){
  var $$obj = p.images;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var img = $$obj[$index];

if ((img.url))
{
buf.push("<img" + (jade.attrs({ 'src':("" + (img.url) + "") }, {"src":true})) + "/>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var img = $$obj[$index];

if ((img.url))
{
buf.push("<img" + (jade.attrs({ 'src':("" + (img.url) + "") }, {"src":true})) + "/>");
}
    }

  }
}).call(this);

buf.push("</div>");
}
buf.push("<div class=\"note-footer clearfix\"><div class=\"pull-right\"><small class=\"text-muted\">" + (jade.escape((jade.interp = Date.create(p.createdOn).relative()) == null ? '' : jade.interp)) + "</small></div><div class=\"post-actions clearfix\"><ul class=\"list-inline action-list\"><li><form" + (jade.attrs({ 'method':("GET"), 'action':("/api/note/" + (p._id) + "/like"), 'data-action':("form") }, {"method":true,"action":true,"data-action":true})) + "><button type=\"submit\" class=\"muted btn btn-plain btn-sm\"><span class=\"fa fa-heart-o\"></span>&nbsp; Like</button></form></li><li><a" + (jade.attrs({ 'href':("#comment_" + (p._id) + ""), "class": [('muted'),('toggleLink'),('btn'),('btn-plain'),('btn-sm')] }, {"href":true})) + "><span class=\"fa fa-reply\"></span>&nbsp; Reply</a></li></ul></div></div><div" + (jade.attrs({ 'id':("comment_" + (p._id) + ""), "class": [('note-comments'),("" + (p.comments.length > 0 ? 'show' : 'hide') + ""),('hide')] }, {"id":true})) + ">");
if ((p.likes.length))
{
buf.push("<div><a href=\"#\"><small><span class=\"fa fa-heart\"></span>&nbsp; " + (jade.escape((jade.interp = p.likes.length) == null ? '' : jade.interp)) + " Likes</small></a></div>");
}
// iterate p.comments
;(function(){
  var $$obj = p.comments;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var c = $$obj[$index];

buf.push("<div class=\"media clearfix\"><a href=\"#\" class=\"pull-left\"><img" + (jade.attrs({ 'src':("" + (c.user.thumbnailUrl) + ""), "class": [('media-object'),('img-circle')] }, {"src":true})) + "/></a><div class=\"media-body\"><strong>" + (jade.escape((jade.interp = c.user.name) == null ? '' : jade.interp)) + "</strong><p class=\"comment-list-body\">" + (jade.escape((jade.interp = c.text) == null ? '' : jade.interp)) + "</p><small style=\"color:#999\" class=\"muted pull-right\">" + (jade.escape((jade.interp = Date.create(c.createDt).relative()) == null ? '' : jade.interp)) + "</small></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var c = $$obj[$index];

buf.push("<div class=\"media clearfix\"><a href=\"#\" class=\"pull-left\"><img" + (jade.attrs({ 'src':("" + (c.user.thumbnailUrl) + ""), "class": [('media-object'),('img-circle')] }, {"src":true})) + "/></a><div class=\"media-body\"><strong>" + (jade.escape((jade.interp = c.user.name) == null ? '' : jade.interp)) + "</strong><p class=\"comment-list-body\">" + (jade.escape((jade.interp = c.text) == null ? '' : jade.interp)) + "</p><small style=\"color:#999\" class=\"muted pull-right\">" + (jade.escape((jade.interp = Date.create(c.createDt).relative()) == null ? '' : jade.interp)) + "</small></div></div>");
    }

  }
}).call(this);

buf.push("<form" + (jade.attrs({ 'method':("post"), 'action':("/note/" + (p._id) + "/comment"), 'data-action':("form"), "class": [('comment')] }, {"method":true,"action":true,"data-action":true})) + "><input" + (jade.attrs({ 'type':("hidden"), 'name':("user"), 'value':("" + (req.user._id) + "") }, {"type":true,"name":true,"value":true})) + "/><div contenteditable=\"true\" data-placeholder=\"Add a comment...\" data-copyto=\"[name='text']\" class=\"textarea form-control\"></div><input type=\"hidden\" name=\"text\"/><div class=\"clearfix footer\"><button type=\"submit\" class=\"btn btn-primary btn-sm pull-right\">Save</button></div></form></div></div><hr/></li>");
}
else if ( p.type == 'joined')
{
buf.push("<li class=\"clearfix\"><div class=\"item-content-holder media-body note-list-body\"><h5><a" + (jade.attrs({ 'href':("/user/" + (p.user._id) + "") }, {"href":true})) + ">" + (jade.escape((jade.interp = p.user.name) == null ? '' : jade.interp)) + "</a>&nbsp;joined the group.</h5></div><hr/></li>");
}
    }

  }
}).call(this);

buf.push("</div>");;return buf.join("");
};

this["App"]["templates"]["_feedPost"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),n = locals_.n;buf.push("<li class=\"clearfix\"><div class=\"media\"><div class=\"pull-left note-list-score hide\"><!--form(method=\"GET\", action=\"/api/note/#{verse[0].osisID}/voteup/#{n._id}\", data-action=\"form\")<button href=\"#\" class=\"btn btn-link\"><span class=\"fa fa-chevron-up\"></span></button>--><div class=\"text-center\">" + (jade.escape((jade.interp = n.scoreText) == null ? '' : jade.interp)) + "</div><!--form(method=\"GET\", action=\"/api/note/#{verse[0].osisID}/votedown/#{n._id}\", data-action=\"form\")<button href=\"#\" class=\"btn btn-link\"><span class=\"fa fa-chevron-down\"></span></button>--></div><div class=\"media-body note-list-body\"><h5 class=\"clearfix\"><a" + (jade.attrs({ 'href':("/user/" + (n.user._id) + ""), 'data-type':('navigator') }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = n.user.name) == null ? '' : jade.interp)) + "</a><a" + (jade.attrs({ 'href':("/user/" + (n.user._id) + ""), 'data-type':('navigator'), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':("" + (n.user.thumbnailUrl) + ""), 'width':("32"), 'style':("margin-left:0"), "class": [('img-circle')] }, {"src":true,"width":true,"style":true})) + "/></a><span>&nbsp;commented on&nbsp;");
if ((n.topicVerses && n.topicVerses.length))
{
buf.push("<strong>" + (jade.escape((jade.interp = n.topicVerses[0].readableName) == null ? '' : jade.interp)) + "</strong>");
}
buf.push("</span></h5><div class=\"note-body-text\">" + (jade.escape((jade.interp = n.text) == null ? '' : jade.interp)) + "</div>");
if ((n.topicVerses && n.topicVerses.length))
{
buf.push("<blockquote><p class=\"text-muted\">" + (jade.escape((jade.interp = n.topicVerses[0].text) == null ? '' : jade.interp)) + "</p></blockquote>");
}
buf.push("<div>" + (jade.escape((jade.interp = n.likeCount) == null ? '' : jade.interp)) + "</div><div><ul class=\"list-inline action-list pull-left\"><li>");
if ((n.topicVerses && n.topicVerses.length))
{
buf.push("<form" + (jade.attrs({ 'method':("GET"), 'action':("/api/note/" + (n.topicVerses[0].osisID) + "/voteup/" + (n._id) + ""), 'data-action':("form"), "class": [('like-form')] }, {"method":true,"action":true,"data-action":true})) + ">");
if ( n.liked)
{
buf.push("<a href=\"#\" class=\"btn btn-plain btn-sm active\"><span style=\"color:#9528AA\" class=\"fa fa-heart\"></span>&nbsp;Liked</a>");
}
else
{
buf.push("<button type=\"submit\" class=\"btn btn-plain btn-sm\"><span style=\"color:green\" class=\"fa fa-heart\"></span>&nbsp;Like</button>");
}
buf.push("</form>");
}
buf.push("</li><li class=\"hide\"><a href=\"#\" class=\"text-muted btn btn-plain btn-sm\"><span class=\"fa fa-facebook\">&nbsp;</span></a></li><li class=\"hide\"><a href=\"#\" class=\"text-muted btn btn-plain btn-sm\"><span class=\"fa fa-twitter\">&nbsp;</span></a></li></ul><small class=\"pull-right text-muted\">" + (jade.escape((jade.interp = Date.create(n.createdOn).relative()) == null ? '' : jade.interp)) + "</small></div></div></div><hr/></li>");;return buf.join("");
};

this["App"]["templates"]["_followButton"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),item = locals_.item,req = locals_.req,isFollowee = locals_.isFollowee;buf.push("<form action=\"/api/follow\" method=\"post\" data-action=\"form\" class=\"followForm pull-right\"><input" + (jade.attrs({ 'type':("hidden"), 'value':("" + (item._id) + ""), 'name':("followee") }, {"type":true,"value":true,"name":true})) + "/><input" + (jade.attrs({ 'type':("hidden"), 'value':("" + (req.user._id) + ""), 'name':("follower") }, {"type":true,"value":true,"name":true})) + "/><input type=\"hidden\" name=\"isUnfollow\" class=\"isUnfollow\"/>");
if ((isFollowee))
{
buf.push("<button type=\"submit\" class=\"btn span12 following\">Following</button>");
}
else
{
buf.push("<button type=\"submit\" class=\"btn btn-primary btn-sm span12 followMe\">Follow</button>");
}
buf.push("</form>");;return buf.join("");
};

this["App"]["templates"]["_friendList"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),friends = locals_.friends;// iterate friends
;(function(){
  var $$obj = friends;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var f = $$obj[$index];

buf.push("<div class=\"clearfix\"><a href=\"#\" class=\"pull-left\"><img" + (jade.attrs({ 'src':("" + (f.picture.data.url) + "") }, {"src":true})) + "/></a><div class=\"media\"><h4>" + (jade.escape((jade.interp = f.name) == null ? '' : jade.interp)) + "</h4></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var f = $$obj[$index];

buf.push("<div class=\"clearfix\"><a href=\"#\" class=\"pull-left\"><img" + (jade.attrs({ 'src':("" + (f.picture.data.url) + "") }, {"src":true})) + "/></a><div class=\"media\"><h4>" + (jade.escape((jade.interp = f.name) == null ? '' : jade.interp)) + "</h4></div></div>");
    }

  }
}).call(this);
;return buf.join("");
};

this["App"]["templates"]["_index"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),user = locals_.user;buf.push("<div class=\"home-feed container\"><div class=\"col-md-8\"><h3>You're logged in!!</h3><a" + (jade.attrs({ 'href':("/user/" + (user._id) + "") }, {"href":true})) + ">See Profile</a></div></div>");;return buf.join("");
};

this["App"]["templates"]["_newnote"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),model = locals_.model,req = locals_.req;buf.push("<div class=\"mainContent\"><div class=\"container\"><div id=\"newNote\"><div data-provides=\"fileupload\" class=\"fileupload fileupload-new center\"><div style=\"max-width: 600px; max-height: 400px; line-height: 20px;\" class=\"fileupload-preview fileupload-exists thumbnail\"></div><div><span class=\"btn btn-file btn-link\"><span style=\"font-size:70px\" data-trigger=\"fileupload\" class=\"glyphicon glyphicon-picture btn-file\"></span><!--form(action=\"/api/note/upload\", method=\"post\", data-action=\"form\", enctype='multipart/form-data').uploadForm--><input type=\"file\" name=\"mainImage\"/></span><!--a(href=\"#\", data-dismiss=\"fileupload\").btn.fileupload-exists Remove--><button type=\"submit\" style=\"margin-left:10px\" class=\"btn btn-primary hide btn fileupload-exists\">Save</button></div></div><form action=\"/api/note/create/\" method=\"POST\" data-action=\"form\" class=\"annotate open\"><input type=\"hidden\" name=\"coverId\"/><div contenteditable=\"true\" data-placeholder=\"Type your title\" data-copyto=\"title\" class=\"noteTitle\"></div><div id=\"toolbar\" style=\"display: none;\"><div class=\"btn-group\"><a data-wysihtml5-command=\"bold\" title=\"CTRL+B\" class=\"btn btn-primary btn-sm\"><span class=\"glyphicon glyphicon-bold\"></span></a><a data-wysihtml5-command=\"italic\" title=\"CTRL+I\" class=\"btn btn-primary btn-sm\"><span class=\"glyphicon glyphicon-italic\"></span></a></div><div style=\"margin-left:10px\" class=\"btn-group\"><a data-wysihtml5-command=\"formatBlock\" data-wysihtml5-command-value=\"h3\" class=\"btn btn-primary btn-sm\"><span class=\"glyphicon glyphicon-header\"></span></a><a data-wysihtml5-command=\"insertUnorderedList\" title=\"CTRL+I\" class=\"btn btn-primary btn-sm\"><span class=\"glyphicon glyphicon-list\"></span></a></div><div style=\"margin-left:10px\" class=\"btn-group\"><a href=\"#\" data-wysihtml5-command=\"insertImage\" data-wysihtml5-command-value=\"http://url.com/foo.jpg\" class=\"btn btn-primary btn-sm\"><span class=\"glyphicon glyphicon-picture\"></span></a></div></div><textarea spellcheck=\"false\" placeholder=\"Is there any word from the lord?\" id=\"textarea\" class=\"textareaBody\"></textarea><input type=\"hidden\" name=\"text\"/><input type=\"hidden\" name=\"title\"/><input type=\"hidden\" name=\"type\" value=\"thought\"/><input type=\"hidden\" name=\"verses\"/><input" + (jade.attrs({ 'type':('hidden'), 'name':("book"), 'value':('' + (model.book) + '') }, {"type":true,"name":true,"value":true})) + "/><input" + (jade.attrs({ 'type':('hidden'), 'name':("chapter"), 'value':('' + (model.chapter) + '') }, {"type":true,"name":true,"value":true})) + "/><input" + (jade.attrs({ 'type':('hidden'), 'name':("group"), 'value':('' + (model.group) + '') }, {"type":true,"name":true,"value":true})) + "/><input" + (jade.attrs({ 'type':('hidden'), 'value':('' + (req.user._id) + ''), 'name':('user') }, {"type":true,"value":true,"name":true})) + "/><div class=\"clearfix\"><button type=\"submit\" class=\"btn btn-info pull-right\">Save</button></div></form></div><script>$(function() {\n  view = new App.Views.NewNote({elm: $('body')});\n  \n})</script></div></div>");;return buf.join("");
};

this["App"]["templates"]["_noteFeeditem"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),p = locals_.p,req = locals_.req;if ( p.type == "note")
{
buf.push("<li class=\"clearfix\"><div class=\"item-content-holder media-body note-list-body\"><h4 style=\"margin:0\" class=\"clearfix\"><a" + (jade.attrs({ 'href':("/user/" + (p.user._id) + ""), 'data-type':('navigator'), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':("" + (p.user.thumbnailUrl) + ""), 'width':("30"), "class": [('img-circle'),('pull-right')] }, {"src":true,"width":true})) + "/></a><a" + (jade.attrs({ 'href':("/user/" + (p.user._id) + ""), 'data-type':('navigator'), "class": [('normal-link')] }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = p.user.name) == null ? '' : jade.interp)) + "</a>");
if ((p.topicVerses && p.topicVerses.length))
{
buf.push("<small>&nbsp;commented on&nbsp;</small><a" + (jade.attrs({ 'href':("/verse/" + (p.topicVerses[0].osisID) + ""), 'data-type':('navigator'), "class": [('normal-link')] }, {"href":true,"data-type":true})) + "><strong>" + (jade.escape((jade.interp = p.topicVerses[0].readableName) == null ? '' : jade.interp)) + "</strong></a>");
}
buf.push("</h4><div class=\"post-snippet note-body-text\">" + (((jade.interp = p.text) == null ? '' : jade.interp)) + "</div>");
if ((p.topicVerses && p.topicVerses.length))
{
buf.push("<blockquote><p class=\"text-muted\">" + (jade.escape((jade.interp = p.topicVerses[0].text) == null ? '' : jade.interp)) + "</p></blockquote>");
}
if ((p.images))
{
buf.push("<div class=\"post-images\">");
// iterate p.images
;(function(){
  var $$obj = p.images;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var img = $$obj[$index];

if ((img.url))
{
buf.push("<img" + (jade.attrs({ 'src':("" + (img.url) + "") }, {"src":true})) + "/>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var img = $$obj[$index];

if ((img.url))
{
buf.push("<img" + (jade.attrs({ 'src':("" + (img.url) + "") }, {"src":true})) + "/>");
}
    }

  }
}).call(this);

buf.push("</div>");
}
buf.push("<div class=\"note-footer clearfix\"><div class=\"pull-right\"><small class=\"text-muted\">" + (jade.escape((jade.interp = Date.create(p.createdOn).relative()) == null ? '' : jade.interp)) + "</small></div><div class=\"post-actions clearfix\"><ul class=\"list-inline action-list\"><li><form" + (jade.attrs({ 'method':("GET"), 'action':("/api/note/" + (p._id) + "/like"), 'data-action':("form") }, {"method":true,"action":true,"data-action":true})) + "><button type=\"submit\" class=\"muted btn btn-plain btn-sm\"><span class=\"fa fa-heart-o\"></span>&nbsp; Like</button></form></li><li><a" + (jade.attrs({ 'href':("#comment_" + (p._id) + ""), "class": [('muted'),('toggleLink'),('btn'),('btn-plain'),('btn-sm')] }, {"href":true})) + "><span class=\"fa fa-reply\"></span>&nbsp; Reply</a></li></ul></div></div><div" + (jade.attrs({ 'id':("comment_" + (p._id) + ""), "class": [('note-comments'),("" + (p.comments.length > 0 ? 'show' : 'hide') + ""),('hide')] }, {"id":true})) + ">");
if ((p.likes.length))
{
buf.push("<div><a href=\"#\"><small><span class=\"fa fa-heart\"></span>&nbsp; " + (jade.escape((jade.interp = p.likes.length) == null ? '' : jade.interp)) + " Likes</small></a></div>");
}
// iterate p.comments
;(function(){
  var $$obj = p.comments;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var c = $$obj[$index];

buf.push("<div class=\"media clearfix\"><a href=\"#\" class=\"pull-left\"><img" + (jade.attrs({ 'src':("" + (c.user.thumbnailUrl) + ""), "class": [('media-object'),('img-circle')] }, {"src":true})) + "/></a><div class=\"media-body\"><strong>" + (jade.escape((jade.interp = c.user.name) == null ? '' : jade.interp)) + "</strong><p class=\"comment-list-body\">" + (jade.escape((jade.interp = c.text) == null ? '' : jade.interp)) + "</p><small style=\"color:#999\" class=\"muted pull-right\">" + (jade.escape((jade.interp = Date.create(c.createDt).relative()) == null ? '' : jade.interp)) + "</small></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var c = $$obj[$index];

buf.push("<div class=\"media clearfix\"><a href=\"#\" class=\"pull-left\"><img" + (jade.attrs({ 'src':("" + (c.user.thumbnailUrl) + ""), "class": [('media-object'),('img-circle')] }, {"src":true})) + "/></a><div class=\"media-body\"><strong>" + (jade.escape((jade.interp = c.user.name) == null ? '' : jade.interp)) + "</strong><p class=\"comment-list-body\">" + (jade.escape((jade.interp = c.text) == null ? '' : jade.interp)) + "</p><small style=\"color:#999\" class=\"muted pull-right\">" + (jade.escape((jade.interp = Date.create(c.createDt).relative()) == null ? '' : jade.interp)) + "</small></div></div>");
    }

  }
}).call(this);

buf.push("<form" + (jade.attrs({ 'method':("post"), 'action':("/note/" + (p._id) + "/comment"), 'data-action':("form"), "class": [('comment')] }, {"method":true,"action":true,"data-action":true})) + "><input" + (jade.attrs({ 'type':("hidden"), 'name':("user"), 'value':("" + (req.user._id) + "") }, {"type":true,"name":true,"value":true})) + "/><div contenteditable=\"true\" data-placeholder=\"Add a comment...\" data-copyto=\"[name='text']\" class=\"textarea form-control\"></div><input type=\"hidden\" name=\"text\"/><div class=\"clearfix footer\"><button type=\"submit\" class=\"btn btn-primary btn-sm pull-right\">Save</button></div></form></div></div><hr/></li>");
}
else if ( p.type == 'joined')
{
buf.push("<li class=\"clearfix\"><div class=\"item-content-holder media-body note-list-body\"><h5><a" + (jade.attrs({ 'href':("/user/" + (p.user._id) + "") }, {"href":true})) + ">" + (jade.escape((jade.interp = p.user.name) == null ? '' : jade.interp)) + "</a>&nbsp;joined the group.</h5></div><hr/></li>");
};return buf.join("");
};

this["App"]["templates"]["_profilepic"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),item = locals_.item,req = locals_.req;if ((item.thumbnailUrl))
{
if ((req.user && req.user._id.toString() == item._id.toString() && !item.disablePhotoEdit))
{
buf.push("<div data-provides=\"fileupload\" class=\"fileupload fileupload-new text-center\"><div class=\"fileupload-new\"><img" + (jade.attrs({ 'src':("" + (item.thumbnailUrl) + ""), "class": [('img-circle'),('profile-pic')] }, {"src":true})) + "/></div><div style=\"line-height: 20px;\" class=\"fileupload-preview fileupload-exists text-center\"></div><div><span class=\"btn btn-file btn-default\"><span class=\"fileupload-new\">Select image</span><span class=\"fileupload-exists\">Change</span><input type=\"file\" name=\"mainImage\" data-target=\"[name=&quot;thumbnailUrl&quot;]\"/></span><!--a(href=\"#\", data-dismiss=\"fileupload\").btn.fileupload-exists Remove--><!--button.btn.btn-primary(type='submit', class=\"btn fileupload-exists\", style='margin-left:10px') Save--></div></div>");
}
else
{
buf.push("<div class=\"text-center\"><img" + (jade.attrs({ 'src':('' + (item.thumbnailUrl) + ''), "class": [('img-circle'),('profile-pic')] }, {"src":true})) + "/></div>");
}
}
else
{
if ((req.user && req.user._id.toString() == item._id.toString()))
{
buf.push("<div data-provides=\"fileupload\" class=\"fileupload fileupload-new span3\"><div style=\"width: 200px; height: 150px;\" class=\"fileupload-preview thumbnail\"><img src=\"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image\"/></div><div><span class=\"btn btn-default btn-file\"><span class=\"fileupload-new\">Select image</span><input type=\"file\" name=\"mainImage\" data-target=\"[name=&quot;thumbnailUrl&quot;]\"/></span></div></div>");
}
else
{
buf.push("<img src=\"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image\" class=\"col-lg-2\"/>");
}
}
buf.push("<form action=\"/user/image\" method=\"post\" data-action=\"form\"><input type=\"hidden\" name=\"thumbnailUrl\"/><button type=\"submit\" class=\"hide\">submit</button></form>");;return buf.join("");
};

this["App"]["templates"]["_searchResults"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),groups = locals_.groups,users = locals_.users,req = locals_.req,isFollowee = locals_.isFollowee,verses = locals_.verses;buf.push("<div class=\"container\"><div class=\"col-md-8 main-content\">");
if ((groups && groups.length))
{
buf.push("<h3>Reading Groups</h3><ul class=\"image-list list-unstyled\">");
// iterate groups
;(function(){
  var $$obj = groups;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var g = $$obj[$index];

buf.push("<li class=\"post clearfix\"><a" + (jade.attrs({ 'href':("/group/" + (g._id) + ""), 'data-type':("navigator"), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':("" + (g.thumbnailUrl) + ""), "class": [('img-circle')] }, {"src":true})) + "/></a><div class=\"post-content media-body\"><h4><a" + (jade.attrs({ 'href':("/group/" + (g._id) + ""), 'data-type':("navigator") }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = g.name) == null ? '' : jade.interp)) + "</a></h4><div class=\"post-snippet\">" + (((jade.interp = g.description.truncate(300)) == null ? '' : jade.interp)) + "</div><div class=\"footer clearfix\"><small class=\"post-meta\">" + (jade.escape((jade.interp = g.members.length || 0) == null ? '' : jade.interp)) + " members</small><small class=\"post-meta pull-right\">" + (jade.escape((jade.interp = g.creator.name) == null ? '' : jade.interp)) + "</small></div></div></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var g = $$obj[$index];

buf.push("<li class=\"post clearfix\"><a" + (jade.attrs({ 'href':("/group/" + (g._id) + ""), 'data-type':("navigator"), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':("" + (g.thumbnailUrl) + ""), "class": [('img-circle')] }, {"src":true})) + "/></a><div class=\"post-content media-body\"><h4><a" + (jade.attrs({ 'href':("/group/" + (g._id) + ""), 'data-type':("navigator") }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = g.name) == null ? '' : jade.interp)) + "</a></h4><div class=\"post-snippet\">" + (((jade.interp = g.description.truncate(300)) == null ? '' : jade.interp)) + "</div><div class=\"footer clearfix\"><small class=\"post-meta\">" + (jade.escape((jade.interp = g.members.length || 0) == null ? '' : jade.interp)) + " members</small><small class=\"post-meta pull-right\">" + (jade.escape((jade.interp = g.creator.name) == null ? '' : jade.interp)) + "</small></div></div></li>");
    }

  }
}).call(this);

buf.push("</ul>");
}
if ((users && users.length))
{
buf.push("<h3>People</h3><hr/><ul class=\"list-unstyled\">");
// iterate users
;(function(){
  var $$obj = users;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var u = $$obj[$index];

buf.push("<li style=\"margin-top:0\" class=\"media\"><a" + (jade.attrs({ 'href':("/user/" + u._id), 'data-type':("navigator"), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':('' + (u.thumbnailUrl) + ''), 'width':("50"), 'height':("50"), "class": [('media-object')] }, {"src":true,"width":true,"height":true})) + "/></a><div class=\"media-body\"><h5 class=\"media-heading\"><a" + (jade.attrs({ 'href':('/user/' + (u._id) + ''), 'data-type':("navigator") }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = u.name) == null ? '' : jade.interp)) + "</a></h5>");
var item = u;
buf.push("<form action=\"/api/follow\" method=\"post\" data-action=\"form\" class=\"followForm pull-right\"><input" + (jade.attrs({ 'type':("hidden"), 'value':("" + (item._id) + ""), 'name':("followee") }, {"type":true,"value":true,"name":true})) + "/><input" + (jade.attrs({ 'type':("hidden"), 'value':("" + (req.user._id) + ""), 'name':("follower") }, {"type":true,"value":true,"name":true})) + "/><input type=\"hidden\" name=\"isUnfollow\" class=\"isUnfollow\"/>");
if ((isFollowee))
{
buf.push("<button type=\"submit\" class=\"btn span12 following\">Following</button>");
}
else
{
buf.push("<button type=\"submit\" class=\"btn btn-primary btn-sm span12 followMe\">Follow</button>");
}
buf.push("</form>");
item =undefined;
buf.push("</div></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var u = $$obj[$index];

buf.push("<li style=\"margin-top:0\" class=\"media\"><a" + (jade.attrs({ 'href':("/user/" + u._id), 'data-type':("navigator"), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':('' + (u.thumbnailUrl) + ''), 'width':("50"), 'height':("50"), "class": [('media-object')] }, {"src":true,"width":true,"height":true})) + "/></a><div class=\"media-body\"><h5 class=\"media-heading\"><a" + (jade.attrs({ 'href':('/user/' + (u._id) + ''), 'data-type':("navigator") }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = u.name) == null ? '' : jade.interp)) + "</a></h5>");
var item = u;
buf.push("<form action=\"/api/follow\" method=\"post\" data-action=\"form\" class=\"followForm pull-right\"><input" + (jade.attrs({ 'type':("hidden"), 'value':("" + (item._id) + ""), 'name':("followee") }, {"type":true,"value":true,"name":true})) + "/><input" + (jade.attrs({ 'type':("hidden"), 'value':("" + (req.user._id) + ""), 'name':("follower") }, {"type":true,"value":true,"name":true})) + "/><input type=\"hidden\" name=\"isUnfollow\" class=\"isUnfollow\"/>");
if ((isFollowee))
{
buf.push("<button type=\"submit\" class=\"btn span12 following\">Following</button>");
}
else
{
buf.push("<button type=\"submit\" class=\"btn btn-primary btn-sm span12 followMe\">Follow</button>");
}
buf.push("</form>");
item =undefined;
buf.push("</div></li>");
    }

  }
}).call(this);

buf.push("</ul>");
}
if ((verses && verses.length))
{
buf.push("<h3>Verses</h3><hr/><ul class=\"list-unstyled verseContainer\">");
// iterate verses
;(function(){
  var $$obj = verses;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var verse = $$obj[$index];

buf.push("<li><div" + (jade.attrs({ 'id':("verse-" + (verse._source.osisID) + ""), "class": [("verse")] }, {"class":true,"id":true})) + "><h4><a" + (jade.attrs({ 'href':("/verse/" + (verse._source.osisID) + ""), 'data-type':("navigator") }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = verse.readableName) == null ? '' : jade.interp)) + "</a></h4><div>" + (((jade.interp = verse.highlight.text) == null ? '' : jade.interp)) + "&nbsp;</div><div class=\"clearfix\"><ul class=\"list-inline action-list\"><li><a href=\"#\" class=\"muted\"><span class=\"glyphicon glyphicon-heart\"></span>&nbsp; Favorite</a></li><li>·</li><li><a href=\"#\" class=\"muted\"><span class=\"glyphicon glyphicon-heart\"></span>&nbsp; Question</a></li><li>·</li><li><a href=\"#\" class=\"muted\"><span class=\"glyphicon glyphicon-tag\"></span>&nbsp; Add Topic</a></li><li>·</li><li><a href=\"#\" class=\"muted\"><span class=\"glyphicon glyphicon-comment\"></span>&nbsp; Note</a></li></ul><ul class=\"list-inline\"><li><a href=\"#\" class=\"muted\">14,500 favorited</a></li><li>·</li><li><a href=\"#\" class=\"muted\">14 questions</a></li><li>·</li><li><a href=\"#\" class=\"muted\">221 notes</a></li></ul></div></div></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var verse = $$obj[$index];

buf.push("<li><div" + (jade.attrs({ 'id':("verse-" + (verse._source.osisID) + ""), "class": [("verse")] }, {"class":true,"id":true})) + "><h4><a" + (jade.attrs({ 'href':("/verse/" + (verse._source.osisID) + ""), 'data-type':("navigator") }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = verse.readableName) == null ? '' : jade.interp)) + "</a></h4><div>" + (((jade.interp = verse.highlight.text) == null ? '' : jade.interp)) + "&nbsp;</div><div class=\"clearfix\"><ul class=\"list-inline action-list\"><li><a href=\"#\" class=\"muted\"><span class=\"glyphicon glyphicon-heart\"></span>&nbsp; Favorite</a></li><li>·</li><li><a href=\"#\" class=\"muted\"><span class=\"glyphicon glyphicon-heart\"></span>&nbsp; Question</a></li><li>·</li><li><a href=\"#\" class=\"muted\"><span class=\"glyphicon glyphicon-tag\"></span>&nbsp; Add Topic</a></li><li>·</li><li><a href=\"#\" class=\"muted\"><span class=\"glyphicon glyphicon-comment\"></span>&nbsp; Note</a></li></ul><ul class=\"list-inline\"><li><a href=\"#\" class=\"muted\">14,500 favorited</a></li><li>·</li><li><a href=\"#\" class=\"muted\">14 questions</a></li><li>·</li><li><a href=\"#\" class=\"muted\">221 notes</a></li></ul></div></div></li>");
    }

  }
}).call(this);

buf.push("</ul>");
}
buf.push("</div><script>$('form.followForm').on('success', function (e, response, statusText, xhr, $form) {\n  if(response.success)\n    $('button', $form).attr('disabled', 'disabled');\n})</script></div>");;return buf.join("");
};

this["App"]["templates"]["_signin"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),message = locals_.message,next = locals_.next;buf.push("<form method=\"post\" action=\"/user/login\" class=\"form-signin\"><h1 class=\"form-signin-heading\">Sign in</h1><a href=\"/verify/facebook\" style=\"margin-bottom: 10px;\" class=\"btn btn-facebook hide\"><span class=\"fa fa-facebook-square\"></span>&nbsp;|&nbsp;Login with facebook</a>");
if ((message && message.length))
{
buf.push("<div class=\"alert alert-danger\">" + (jade.escape((jade.interp = message) == null ? '' : jade.interp)) + "</div>");
}
if ( (next))
{
buf.push("<input" + (jade.attrs({ 'type':("hidden"), 'value':("" + (next) + ""), 'name':("next") }, {"type":true,"value":true,"name":true})) + "/>");
}
buf.push("<input type=\"text\" name=\"email\" placeholder=\"Email\" class=\"form-control\"/><input type=\"password\" name=\"password\" placeholder=\"Password\" class=\"form-control\"/><div class=\"forgot-password\"><a href=\"/user/forgotpassword\">Forgot Password?</a></div><div style=\"margin-top:20px\"><button type=\"submit\" class=\"btn btn-default btn-lg btn-block\">Login</button></div></form>");;return buf.join("");
};

this["App"]["templates"]["_signup"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),group = locals_.group,title = locals_.title,isIe = locals_.isIe,token = locals_.token,next = locals_.next;buf.push("<div class=\"container\"><div class=\"home-signedout\">");
if ((!group))
{
buf.push("<h1 class=\"form-signin-heading text-center text-muted\">" + (jade.escape((jade.interp = title) == null ? '' : jade.interp)) + "</h1>");
}
else
{
buf.push("<h1 class=\"form-signin-heading text-center text-muted\">Sign up to be a member of<br/><span style=\"color:#1E9908\">&nbsp; " + (jade.escape((jade.interp = group.name) == null ? '' : jade.interp)) + "</span></h1>");
}
buf.push("<form method=\"post\" action=\"/api/user/create\" data-action=\"form\" class=\"form-signup\"><div class=\"alert alert-danger hide\"><div class=\"message\"></div></div>");
if ((isIe))
{
buf.push("<label>Name:</label>");
}
buf.push("<input type=\"text\" name=\"name\" placeholder=\"Name\" class=\"form-control first-input\"/>");
if ((isIe))
{
buf.push("<label>Password:</label>");
}
buf.push("<input type=\"password\" name=\"password\" placeholder=\"Password\" class=\"form-control\"/>");
if ((isIe))
{
buf.push("<label>Email:</label>");
}
buf.push("<input type=\"text\" name=\"email\" placeholder=\"Email\" class=\"form-control last-input\"/>");
if ((token))
{
buf.push("<input" + (jade.attrs({ 'type':('hidden'), 'name':('token'), 'value':("" + (token) + "") }, {"type":true,"name":true,"value":true})) + "/>");
}
buf.push("<div style=\"margin-top:20px\"><button type=\"submit\" class=\"btn btn-default btn-lg btn-block\">Register</button></div><div style=\"margin-top:20px;\">Already have an account?");
console.log(next)
if ((next))
{
buf.push("<a" + (jade.attrs({ 'href':("/login?next=" + (next) + ""), 'data-type':("navigator") }, {"href":true,"data-type":true})) + ">&nbsp;Log in now</a>");
}
else
{
buf.push("<a href=\"/login\" data-type=\"navigator\">&nbsp;Log in now</a>");
}
buf.push("</div></form></div></div>");;return buf.join("");
};

this["App"]["templates"]["_useredit"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),item = locals_.item,req = locals_.req;buf.push("<div class=\"container user-page\"><div class=\"main-content col-md-12\"><div class=\"clearfix\">");
if ((item.thumbnailUrl))
{
if ((req.user && req.user._id.toString() == item._id.toString() && !item.disablePhotoEdit))
{
buf.push("<div data-provides=\"fileupload\" class=\"fileupload fileupload-new text-center\"><div class=\"fileupload-new\"><img" + (jade.attrs({ 'src':("" + (item.thumbnailUrl) + ""), "class": [('img-circle'),('profile-pic')] }, {"src":true})) + "/></div><div style=\"line-height: 20px;\" class=\"fileupload-preview fileupload-exists text-center\"></div><div><span class=\"btn btn-file btn-default\"><span class=\"fileupload-new\">Select image</span><span class=\"fileupload-exists\">Change</span><input type=\"file\" name=\"mainImage\" data-target=\"[name=&quot;thumbnailUrl&quot;]\"/></span><!--a(href=\"#\", data-dismiss=\"fileupload\").btn.fileupload-exists Remove--><!--button.btn.btn-primary(type='submit', class=\"btn fileupload-exists\", style='margin-left:10px') Save--></div></div>");
}
else
{
buf.push("<div class=\"text-center\"><img" + (jade.attrs({ 'src':('' + (item.thumbnailUrl) + ''), "class": [('img-circle'),('profile-pic')] }, {"src":true})) + "/></div>");
}
}
else
{
if ((req.user && req.user._id.toString() == item._id.toString()))
{
buf.push("<div data-provides=\"fileupload\" class=\"fileupload fileupload-new span3\"><div style=\"width: 200px; height: 150px;\" class=\"fileupload-preview thumbnail\"><img src=\"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image\"/></div><div><span class=\"btn btn-default btn-file\"><span class=\"fileupload-new\">Select image</span><input type=\"file\" name=\"mainImage\" data-target=\"[name=&quot;thumbnailUrl&quot;]\"/></span></div></div>");
}
else
{
buf.push("<img src=\"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image\" class=\"col-lg-2\"/>");
}
}
buf.push("<form action=\"/user/image\" method=\"post\" data-action=\"form\"><input type=\"hidden\" name=\"thumbnailUrl\"/><button type=\"submit\" class=\"hide\">submit</button></form><form" + (jade.attrs({ 'method':("put"), 'action':("/api/user/" + (item._id) + "/update"), 'data-action':("form"), "class": [('form-horizontal')] }, {"method":true,"action":true,"data-action":true})) + "><div class=\"form-group\"><div class=\"col-sm-3 control-label\"><strong>Name:</strong></div><div class=\"col-sm-9\"><input" + (jade.attrs({ 'name':("name"), 'value':("" + (item.name) + ""), "class": [('form-control')] }, {"name":true,"value":true})) + "/><div class=\"help-block\">Name that will appear in search</div></div></div><div class=\"form-group\"><div class=\"col-sm-3 control-label\"><strong>Email:</strong></div><div class=\"col-sm-9\"><input" + (jade.attrs({ 'name':("email"), 'value':("" + (item.email) + ""), "class": [('form-control')] }, {"name":true,"value":true})) + "/><div class=\"help-block\">Will not be displayed publically</div></div></div><div class=\"form-group\"><div class=\"col-sm-3 control-label\"><strong>Denomiation:</strong></div><div class=\"col-sm-9\"><input" + (jade.attrs({ 'name':("denomination"), 'value':("" + (item.denomination) + ""), "class": [('form-control')] }, {"name":true,"value":true})) + "/><div class=\"help-block\">Denominational views. It's ok to leave blank.</div></div></div><div class=\"clearfix\"><button type=\"submit\" class=\"btn btn-primary pull-right\">Save</button></div></form></div></div></div>");;return buf.join("");
};

this["App"]["templates"]["_usershow"] = function anonymous(locals) {
var buf = [];
var locals_ = (locals || {}),req = locals_.req,item = locals_.item,isFollowee = locals_.isFollowee,tab = locals_.tab,posts = locals_.posts,following = locals_.following,followers = locals_.followers;buf.push("<div class=\"container user-page\"><div class=\"clearfix\"><div class=\"btn-group pull-right\">");
if ((req.user && req.user._id.toString() == item._id.toString()))
{
buf.push("<a" + (jade.attrs({ 'href':("/user/" + (item._id) + "/edit"), "class": [('btn'),('btn-default'),('btn-xs')] }, {"href":true})) + ">Edit Info</a><a href=\"/user/logout\" class=\"btn btn-default btn-xs\">Sign Out</a>");
}
buf.push("</div></div><div class=\"col-md-4 text-center profile-section\"><div class=\"clearfix\">");
item.disablePhotoEdit = true;
if ((item.thumbnailUrl))
{
if ((req.user && req.user._id.toString() == item._id.toString() && !item.disablePhotoEdit))
{
buf.push("<div data-provides=\"fileupload\" class=\"fileupload fileupload-new text-center\"><div class=\"fileupload-new\"><img" + (jade.attrs({ 'src':("" + (item.thumbnailUrl) + ""), "class": [('img-circle'),('profile-pic')] }, {"src":true})) + "/></div><div style=\"line-height: 20px;\" class=\"fileupload-preview fileupload-exists text-center\"></div><div><span class=\"btn btn-file btn-default\"><span class=\"fileupload-new\">Select image</span><span class=\"fileupload-exists\">Change</span><input type=\"file\" name=\"mainImage\" data-target=\"[name=&quot;thumbnailUrl&quot;]\"/></span><!--a(href=\"#\", data-dismiss=\"fileupload\").btn.fileupload-exists Remove--><!--button.btn.btn-primary(type='submit', class=\"btn fileupload-exists\", style='margin-left:10px') Save--></div></div>");
}
else
{
buf.push("<div class=\"text-center\"><img" + (jade.attrs({ 'src':('' + (item.thumbnailUrl) + ''), "class": [('img-circle'),('profile-pic')] }, {"src":true})) + "/></div>");
}
}
else
{
if ((req.user && req.user._id.toString() == item._id.toString()))
{
buf.push("<div data-provides=\"fileupload\" class=\"fileupload fileupload-new span3\"><div style=\"width: 200px; height: 150px;\" class=\"fileupload-preview thumbnail\"><img src=\"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image\"/></div><div><span class=\"btn btn-default btn-file\"><span class=\"fileupload-new\">Select image</span><input type=\"file\" name=\"mainImage\" data-target=\"[name=&quot;thumbnailUrl&quot;]\"/></span></div></div>");
}
else
{
buf.push("<img src=\"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image\" class=\"col-lg-2\"/>");
}
}
buf.push("<form action=\"/user/image\" method=\"post\" data-action=\"form\"><input type=\"hidden\" name=\"thumbnailUrl\"/><button type=\"submit\" class=\"hide\">submit</button></form><h3 class=\"text-center\">" + (jade.escape((jade.interp = item.name) == null ? '' : jade.interp)) + "");
if ((!req.user || req.user._id.toString() != item._id.toString()))
{
buf.push("<form action=\"/api/follow\" method=\"post\" data-action=\"form\" class=\"followForm pull-right\"><input" + (jade.attrs({ 'type':("hidden"), 'value':("" + (item._id) + ""), 'name':("followee") }, {"type":true,"value":true,"name":true})) + "/><input" + (jade.attrs({ 'type':("hidden"), 'value':("" + (req.user._id) + ""), 'name':("follower") }, {"type":true,"value":true,"name":true})) + "/><input type=\"hidden\" name=\"isUnfollow\" class=\"isUnfollow\"/>");
if ((isFollowee))
{
buf.push("<button type=\"submit\" class=\"btn span12 following\">Following</button>");
}
else
{
buf.push("<button type=\"submit\" class=\"btn btn-primary btn-sm span12 followMe\">Follow</button>");
}
buf.push("</form>");
}
buf.push("</h3><ul class=\"list-inline\"><li style=\"border-right:solid 3px #f1f1f1\"><div><h4 style=\"font-size:20px\" class=\"text-center\"><a" + (jade.attrs({ 'href':("/user/" + (item._id) + ""), 'data-type':("navigator"), "class": [('normal-link')] }, {"href":true,"data-type":true})) + ">10</a></h4><div style=\"font-size:13px\" class=\"text-muted\"><a" + (jade.attrs({ 'href':("/user/" + (item._id) + ""), 'data-type':("navigator"), "class": [('text-muted')] }, {"href":true,"data-type":true})) + ">Posts</a></div></div></li></ul></div></div><div class=\"col-md-8\"><div style=\"margin-top:0;\" class=\"lv-panel\"><div class=\"lv-panel-inner\">");
if ( tab == 'posts')
{
if ( !posts.length)
{
buf.push("<div class=\"alert alert-warning\"><h5>No Posts</h5></div>");
}
buf.push("<ul data-more-handler=\"true\" class=\"list-unstyled note-list\">");
// iterate posts
;(function(){
  var $$obj = posts;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var n = $$obj[$index];

buf.push("<li class=\"clearfix\"><div class=\"media\"><div class=\"pull-left note-list-score hide\"><!--form(method=\"GET\", action=\"/api/note/#{verse[0].osisID}/voteup/#{n._id}\", data-action=\"form\")<button href=\"#\" class=\"btn btn-link\"><span class=\"fa fa-chevron-up\"></span></button>--><div class=\"text-center\">" + (jade.escape((jade.interp = n.scoreText) == null ? '' : jade.interp)) + "</div><!--form(method=\"GET\", action=\"/api/note/#{verse[0].osisID}/votedown/#{n._id}\", data-action=\"form\")<button href=\"#\" class=\"btn btn-link\"><span class=\"fa fa-chevron-down\"></span></button>--></div><div class=\"media-body note-list-body\"><h5 class=\"clearfix\"><a" + (jade.attrs({ 'href':("/user/" + (n.user._id) + ""), 'data-type':('navigator') }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = n.user.name) == null ? '' : jade.interp)) + "</a><a" + (jade.attrs({ 'href':("/user/" + (n.user._id) + ""), 'data-type':('navigator'), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':("" + (n.user.thumbnailUrl) + ""), 'width':("32"), 'style':("margin-left:0"), "class": [('img-circle')] }, {"src":true,"width":true,"style":true})) + "/></a><span>&nbsp;commented on&nbsp;");
if ((n.topicVerses && n.topicVerses.length))
{
buf.push("<strong>" + (jade.escape((jade.interp = n.topicVerses[0].readableName) == null ? '' : jade.interp)) + "</strong>");
}
buf.push("</span></h5><div class=\"note-body-text\">" + (jade.escape((jade.interp = n.text) == null ? '' : jade.interp)) + "</div>");
if ((n.topicVerses && n.topicVerses.length))
{
buf.push("<blockquote><p class=\"text-muted\">" + (jade.escape((jade.interp = n.topicVerses[0].text) == null ? '' : jade.interp)) + "</p></blockquote>");
}
buf.push("<div>" + (jade.escape((jade.interp = n.likeCount) == null ? '' : jade.interp)) + "</div><div><ul class=\"list-inline action-list pull-left\"><li>");
if ((n.topicVerses && n.topicVerses.length))
{
buf.push("<form" + (jade.attrs({ 'method':("GET"), 'action':("/api/note/" + (n.topicVerses[0].osisID) + "/voteup/" + (n._id) + ""), 'data-action':("form"), "class": [('like-form')] }, {"method":true,"action":true,"data-action":true})) + ">");
if ( n.liked)
{
buf.push("<a href=\"#\" class=\"btn btn-plain btn-sm active\"><span style=\"color:#9528AA\" class=\"fa fa-heart\"></span>&nbsp;Liked</a>");
}
else
{
buf.push("<button type=\"submit\" class=\"btn btn-plain btn-sm\"><span style=\"color:green\" class=\"fa fa-heart\"></span>&nbsp;Like</button>");
}
buf.push("</form>");
}
buf.push("</li><li class=\"hide\"><a href=\"#\" class=\"text-muted btn btn-plain btn-sm\"><span class=\"fa fa-facebook\">&nbsp;</span></a></li><li class=\"hide\"><a href=\"#\" class=\"text-muted btn btn-plain btn-sm\"><span class=\"fa fa-twitter\">&nbsp;</span></a></li></ul><small class=\"pull-right text-muted\">" + (jade.escape((jade.interp = Date.create(n.createdOn).relative()) == null ? '' : jade.interp)) + "</small></div></div></div><hr/></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var n = $$obj[$index];

buf.push("<li class=\"clearfix\"><div class=\"media\"><div class=\"pull-left note-list-score hide\"><!--form(method=\"GET\", action=\"/api/note/#{verse[0].osisID}/voteup/#{n._id}\", data-action=\"form\")<button href=\"#\" class=\"btn btn-link\"><span class=\"fa fa-chevron-up\"></span></button>--><div class=\"text-center\">" + (jade.escape((jade.interp = n.scoreText) == null ? '' : jade.interp)) + "</div><!--form(method=\"GET\", action=\"/api/note/#{verse[0].osisID}/votedown/#{n._id}\", data-action=\"form\")<button href=\"#\" class=\"btn btn-link\"><span class=\"fa fa-chevron-down\"></span></button>--></div><div class=\"media-body note-list-body\"><h5 class=\"clearfix\"><a" + (jade.attrs({ 'href':("/user/" + (n.user._id) + ""), 'data-type':('navigator') }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = n.user.name) == null ? '' : jade.interp)) + "</a><a" + (jade.attrs({ 'href':("/user/" + (n.user._id) + ""), 'data-type':('navigator'), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':("" + (n.user.thumbnailUrl) + ""), 'width':("32"), 'style':("margin-left:0"), "class": [('img-circle')] }, {"src":true,"width":true,"style":true})) + "/></a><span>&nbsp;commented on&nbsp;");
if ((n.topicVerses && n.topicVerses.length))
{
buf.push("<strong>" + (jade.escape((jade.interp = n.topicVerses[0].readableName) == null ? '' : jade.interp)) + "</strong>");
}
buf.push("</span></h5><div class=\"note-body-text\">" + (jade.escape((jade.interp = n.text) == null ? '' : jade.interp)) + "</div>");
if ((n.topicVerses && n.topicVerses.length))
{
buf.push("<blockquote><p class=\"text-muted\">" + (jade.escape((jade.interp = n.topicVerses[0].text) == null ? '' : jade.interp)) + "</p></blockquote>");
}
buf.push("<div>" + (jade.escape((jade.interp = n.likeCount) == null ? '' : jade.interp)) + "</div><div><ul class=\"list-inline action-list pull-left\"><li>");
if ((n.topicVerses && n.topicVerses.length))
{
buf.push("<form" + (jade.attrs({ 'method':("GET"), 'action':("/api/note/" + (n.topicVerses[0].osisID) + "/voteup/" + (n._id) + ""), 'data-action':("form"), "class": [('like-form')] }, {"method":true,"action":true,"data-action":true})) + ">");
if ( n.liked)
{
buf.push("<a href=\"#\" class=\"btn btn-plain btn-sm active\"><span style=\"color:#9528AA\" class=\"fa fa-heart\"></span>&nbsp;Liked</a>");
}
else
{
buf.push("<button type=\"submit\" class=\"btn btn-plain btn-sm\"><span style=\"color:green\" class=\"fa fa-heart\"></span>&nbsp;Like</button>");
}
buf.push("</form>");
}
buf.push("</li><li class=\"hide\"><a href=\"#\" class=\"text-muted btn btn-plain btn-sm\"><span class=\"fa fa-facebook\">&nbsp;</span></a></li><li class=\"hide\"><a href=\"#\" class=\"text-muted btn btn-plain btn-sm\"><span class=\"fa fa-twitter\">&nbsp;</span></a></li></ul><small class=\"pull-right text-muted\">" + (jade.escape((jade.interp = Date.create(n.createdOn).relative()) == null ? '' : jade.interp)) + "</small></div></div></div><hr/></li>");
    }

  }
}).call(this);

buf.push("</ul>");
}
if ( tab == 'following')
{
buf.push("<ul class=\"list-unstyled follow-list\">");
// iterate following
;(function(){
  var $$obj = following;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var p = $$obj[$index];

buf.push("<li><div class=\"clearfix\"><a" + (jade.attrs({ 'href':("/user/" + p._id), 'data-type':("navigator"), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':('' + (p.thumbnailUrl) + ''), 'width':("50"), 'height':("50"), "class": [('img-circle')] }, {"src":true,"width":true,"height":true})) + "/></a><div class=\"media-body\"><h5 class=\"media-heading\"><a" + (jade.attrs({ 'href':("/user/" + (p._id) + ""), 'data-type':("navigator"), "class": [('normal-link')] }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = p.name) == null ? '' : jade.interp)) + "</a></h5></div></div><hr/></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var p = $$obj[$index];

buf.push("<li><div class=\"clearfix\"><a" + (jade.attrs({ 'href':("/user/" + p._id), 'data-type':("navigator"), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':('' + (p.thumbnailUrl) + ''), 'width':("50"), 'height':("50"), "class": [('img-circle')] }, {"src":true,"width":true,"height":true})) + "/></a><div class=\"media-body\"><h5 class=\"media-heading\"><a" + (jade.attrs({ 'href':("/user/" + (p._id) + ""), 'data-type':("navigator"), "class": [('normal-link')] }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = p.name) == null ? '' : jade.interp)) + "</a></h5></div></div><hr/></li>");
    }

  }
}).call(this);

buf.push("</ul>");
}
if ( tab == 'followers')
{
buf.push("<ul class=\"list-unstyled follow-list\">");
// iterate followers
;(function(){
  var $$obj = followers;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var p = $$obj[$index];

buf.push("<li><div class=\"clearfix\"><a" + (jade.attrs({ 'href':("/user/" + p._id), 'data-type':("navigator"), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':('' + (p.thumbnailUrl) + ''), 'width':("50"), 'height':("50"), "class": [('img-circle')] }, {"src":true,"width":true,"height":true})) + "/></a><div class=\"media-body\"><h5 class=\"media-heading\"><a" + (jade.attrs({ 'href':("/user/" + (p._id) + ""), 'data-type':("navigator") }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = p.name) == null ? '' : jade.interp)) + "</a></h5></div></div><hr/></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var p = $$obj[$index];

buf.push("<li><div class=\"clearfix\"><a" + (jade.attrs({ 'href':("/user/" + p._id), 'data-type':("navigator"), "class": [('pull-left')] }, {"href":true,"data-type":true})) + "><img" + (jade.attrs({ 'src':('' + (p.thumbnailUrl) + ''), 'width':("50"), 'height':("50"), "class": [('img-circle')] }, {"src":true,"width":true,"height":true})) + "/></a><div class=\"media-body\"><h5 class=\"media-heading\"><a" + (jade.attrs({ 'href':("/user/" + (p._id) + ""), 'data-type':("navigator") }, {"href":true,"data-type":true})) + ">" + (jade.escape((jade.interp = p.name) == null ? '' : jade.interp)) + "</a></h5></div></div><hr/></li>");
    }

  }
}).call(this);

buf.push("</ul>");
}
if ( tab == 'favorites')
{
buf.push("<h3>Favorites</h3>");
}
buf.push("</div></div></div></div>");;return buf.join("");
};
App.SocialMedia = Class.extend({
	fetchFacebook: function (verse, cb) {
		App.ajax.get('/api/bible/social', {q: verse}, cb)
	}
})

App.facebookSearchView = App.View.extend({
	bindEvents: function () {

	},

	init: function () {
		var me = this;
		this._super();
		this.model.fetchFacebook(me.verse, function (response) {
			var response = JSON.parse(response);
			response.data.each(function (item) {
				item.message = item.message.truncate(150)
			})
			var template = Mustache.render(me.tmpl, response);
			$(me.elm).html(template)
		});
	}
})
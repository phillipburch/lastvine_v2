App.Models.Search = App.BaseModel.extend({
  entity: 'question',
  search: function(query, cb) {
    $.get('/api/search/?q='+ query, cb)
  }
})

App.Controllers.Search = App.BaseController.extend({
  routes: {
    '/search{?query}': 'search'
  },

  search: function(query) {

    var model = new App.Models.Search();
    var me = this;
    model.search(query.q, function(response) {
      if(response.isRedirect)
        return crossroads.navigator.navigate(response.redirect)
      if(response.verseModel)
        me._showPage('_verseSearchResults', response)
      else
        me._showPage('_searchResults', response)
    })

  }
});
new App.Controllers.Search();
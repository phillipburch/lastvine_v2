App.Models.home = App.BaseModel.extend({
  entity: 'home'
})

App.Models.feed = App.BaseModel.extend({
  entity: 'feed'
})

App.Controllers.home = App.BaseController.extend({
  routes: {
    '/': 'list',
    '/feed': 'feed'
  },

  feed: function() {
    var model = new App.Models.feed();
    var me = this;
    model.fetch(function(response) {
      me._showPage('_homefeed', response)
    });
  },

  new: function() {
    this._showPage('_newgroup', {})
  },

  list: function () {
    if(!App.User)
      return location.reload();

    var model = new App.Models.home();
    var me = this;
    model.fetch(function(response) {
      me._showPage('_index', response);
    })
  },
  show: function(id) {
    var model = new App.Models.group();
    var me = this;
    model.fetchOne(id, function(response) {
      me._showPage('_showgroup', response)
    })
  }
});

new App.Controllers.home();
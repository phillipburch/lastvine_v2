
/*
 * GET home page.
 */
var redis = require('redis')
  , mongoose = require('mongoose')
  , Annotation = mongoose.model('Annotation')
  , ActivityLog = mongoose.model('ActivityLog')
  , bibleService = require('../service/bible')
  , request = require('request');

var service = {
  hydrate: function (idsWithOrdinal, fields, type, cb) {
    var me = this;
    var ids = idsWithOrdinal.map('id');

    ActivityLog.find({_id : {'$in' : ids}}, fields)
      .populate('user', 'name thumbnailUrl')
      .exec(function(err, items) {
        if(err) console.log(err);
        if(err || !items) return cb([])

        var orderedItems = [];
        ids.each(function(id, idx) {
          var item = items.find(function(i) {return i._id == id})
          if(item) {
            item.ordinal = idsWithOrdinal[idx].ordinal;
            item.type = type;
            orderedItems.push(item);
          }
        })
        cb(orderedItems)
      })
  }
};

// Make class a singleton
module.exports = service;
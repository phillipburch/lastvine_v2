var  config = require('../config/config')['development']
  , mongoose = require('mongoose')
  , User = mongoose.model('User')
  , elastical = require('elastical');

var service = {
  _getUsersByIds: function(ids, cb) {
    if(!ids.length)
      return cb(null, []);

    User.find({_id: {'$in': ids}}).exec(function(err, items) {
      cb(err, items)
    })
  },

  _addUserToAnnotations: function(annotations, users) {
    var annotations = annotations.map('_source');
    annotations = annotations.each(function(annotation) {
      annotation.user = users.find(function(u) {return annotation.user == u._id.toString()})
    })
    return annotations;
  },

  group: function(options, cb) {
    var client = new elastical.Client(AppConfig.elasticSearch.host, {port: AppConfig.elasticSearch.port});
    var me = this;

    client.search(options, function(err, body, rs) {
      console.log(body)
      //var annotations = body.hits.filter(function(item) { return item._type == 'note' });
      //var uids = annotations.map(function(i) {return i._source.user });

      //me._getUsersByIds(uids, function(err, users){
      var result = {
        groups: body.hits.filter(function(item) { return item._type == 'group' }).map('_source'),
        users: body.hits.filter(function(item) { return item._type == 'User' }).map('_source'),
        verses: body.hits.filter(function(item) { return item._type == 'verse' }),
        //annotations: me._addUserToAnnotations(annotations, users)
        annotations: []
      }
      cb(err, result)
    })
  },

  searchAll: function(options, cb) {
    var client = new elastical.Client(AppConfig.elasticSearch.host, {port: AppConfig.elasticSearch.port});
    var me = this;

    client.search(options, function(err, body, rs) {
      //console.log(arguments)
      var annotations = body.hits.filter(function(item) { return item._type == 'note' });
      var uids = annotations.map(function(i) {return i._source.user });

      //me._getUsersByIds(uids, function(err, users){
        var result = {
          groups: body.hits.filter(function(item) { return item._type == 'group' }).map('_source'),
          users: body.hits.filter(function(item) { return item._type == 'User' }).map('_source'),
          verses: body.hits.filter(function(item) { return item._type == 'verse' }),
          //annotations: me._addUserToAnnotations(annotations, users)
          //annotations: []
        }
        cb(err, result)
      //})
    })
  }
}

module.exports = service;


/*
 * GET home page.
 */
var  redis = require('redis')
  , mongoose = require('mongoose')
  , Group = mongoose.model('Group')
  , User = mongoose.model('User')
  , Notification = mongoose.model('Notification')
  , emailService = require('../service/email')
  , request = require('request');

var service = {
  notifyGroup: function (groupId, note) {
    var client = redis.createClient()
    console.log(note)
    Group.findOne({_id: groupId}).populate('members', 'name thumbnailUrl').exec(function(err, group) {
      group.members.each(function(member) {
        var notification = new Notification({
          actor: note.user._id,
          type: 'newNote',
          user: member,
          note: note._id,
          group: groupId
        })

        notification.save(function(err, notification) {
          client.zadd('notification:user:' + member._id, Date.now(), notification._id)
          User.update({_id: member._id}, { $inc: { unreadNotificationCount: 1 } }).exec();
          notification = notification.toObject();
          notification.actor = note.user;
          emailService.newNote(member.email, {notification: notification, group: group}, 'newnote')
        })
      })
    })
  }
};

// Make class a singleton
module.exports = service;
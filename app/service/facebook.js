/*
 * GET home page.
 */
var  config = require('../config/config')['development']
	, FB = require('fb');

var service = {
	post: function(body, accessToken) {
		FB.setAccessToken(accessToken);
		FB.api('me/feed', 'post', { message: body}, function (res) {
		  if(!res || res.error) {
		    console.log(!res ? 'error occurred' : res.error);
		    return;
		  }
		  console.log('Post Id: ' + res.id);
		});
	}
};

// Make class a singleton
module.exports = service;
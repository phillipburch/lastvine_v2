var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  when = require('when');

module.exports = {
  getUser: function (id, fields) {
    return when(User.findOne({_id: id}, fields).exec());
  },
  getByToken: function(token) {
    return when(User.findOne({token: token}).exec());
  }
};
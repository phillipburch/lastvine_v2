var fs = require('fs')
	, im = require('imagemagick')
	, util = require('util')
	, path = require('path');


// Handles saving images to disk in various formats.
var service = {
	save: function(dir, name, width, height, file, cb) {
		// This will likely live on another server with a image.lastvine.com domain name and we can structure the directories better
		var path = AppConfig.root + '/public/images/' + dir + '/',
			me = this,
	    tmp_path = file.path,
	    new_path = path + name
	    formatToExtension ={'JPEG': '.jpg', 'PNG': '.png', 'GIF' : '.gif'};

	    var i = tmp_path.lastIndexOf('.');

	    // Probably needs to be asynch
	    if(!fs.existsSync(path)) {
	      fs.mkdirSync(path);
	    }

	    im.identify(tmp_path, function(err, features) {
	    	var extension = formatToExtension[features.format];
	    	if(!extension) return console.error('NOT AN EXCEPTED FILE EXTENSION: ' + features.format);
	    	if(err) return console.log(err);
	    	im.crop({
		      srcPath: tmp_path,
		      dstPath: new_path + extension,
		      gravity: 'North',
		      width:   300,
		      height: 300
		    }, function(err, stdout, stderr){
		      if (err) throw err;
		      cb('/images/' + dir + '/' + name + extension);
		      fs.unlink(tmp_path, function() {
	            if (err) throw err;
	        });
		    });
	    })
	  }
};

// Make class a singleton
module.exports = service;
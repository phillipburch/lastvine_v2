
/*
 * GET home page.
 */
var  config = require('../config/config')['development']
	, client = require('stripe')(config.stripe.secret);

var service = {
	createCustomer: function(token, cb) {
		client.customers.create(
		   { card: token },
		   function(err, customer) {
		      if (err) {
		         console.log(err.message);
		         return;
		      }
		      cb(customer)
		   }
		 );
	},

	charge: function(amount, customer, cb) {
		client.charges.create({
			amount: amount,
			customer: customer,
			currency: 'usd'
		}, function(err, responseData) {
			if (err) {
		        console.log(err.message);
		        return;
		    }
		   cb(responseData)
		});
	},

	refund: function(id, amount, cb) {
		client.charges.refund(id, amount, function(err, responseData) {
			console.log(responseData)
			if (err) {
		        console.log(err.message);
		        return;
		    }
		   cb(responseData)
		});
	}

};

// Make class a singleton
module.exports = service;
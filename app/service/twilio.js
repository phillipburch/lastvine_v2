
/*
 * GET home page.
 */
var client = require('twilio')(AppConfig.twilio.sid, AppConfig.twilio.authToken);

var service = {
	makeShortCode: function(length, phone, cb) {
    // Make a short code and a long code for the group in the invitecodes hash.
    // When users signup to join a group, they need either a short or a long code to do so.
    var redisClient  = redis.createClient();
    var me = this;
    var code = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for( var i=0; i < length; i++ )
        code += possible.charAt(Math.floor(Math.random() * possible.length));

    client.hsetnx('register:codes', code, phone, function(err, response) {
      if(response)
        cb(code)
      else
        me.makeShortCode(length, cb)
    })
  },

	verifySMS: function(number, cb) {
		var me = this;
		me.makeShortCode(4, number, function(code) {
			client.sendSms({
			    to: number, // Any number Twilio can deliver to
			    from: AppConfig.twilio.phone, // A number you bought from Twilio and can use for outbound communication
			    body: 'Your LastVine code is ' + code + '. You can also tap this link: lastvine.com/' + code  // body of the SMS message
			}, function(err, responseData) { //this function is executed when a response is received from Twilio
			    cb(responseData)
			});
		})
	},

	call: function(number, cb) {
		client.makeCall({
		    to: number, // Any number Twilio can call
		    from: AppConfig.twilio.phone, // A number you bought from Twilio and can use for outbound communication
		    url: 'http://www.example.com/twiml.php' // A URL that produces an XML document (TwiML) which contains instructions for the call
		}, function(err, responseData) {
		   cb(responseData)
		});
	}

};

// Make class a singleton
module.exports = service;
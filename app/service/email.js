
/*
 * GET home page.
 */
var  nodemailer = require("nodemailer")
  , jade = require('jade')
  , redis = require('redis')
  , mongoose = require('mongoose')
  , EmailLog = mongoose.model('EmailLog')
  , path = require('path');

var service = {
  _parseTemplate: function( model, template, cb) {
    model.rootUrl = AppConfig.rootUrl;
    jade.renderFile(path.join(AppConfig.root, 'views', 'mailtemplates', template + '.jade'),   model, cb)
  },

  canEmail: function(email, cb) {
    var client = redis.createClient();
    client.sismember('unsubscribed:email', email, function(err, response){
      if(response)
        cb(false)
      else
        cb(true)
    })
  },

  _getTransport: function() {
    return nodemailer.createTransport("SMTP",{
        service: "Mandrill", // sets automatically host, port and connection security settings
        auth: {
            user: AppConfig.mandrill.username,
            pass: AppConfig.mandrill.password
        }
    });
  },

  confirmation: function(model, template) {
    var transport = this._getTransport();
    this._parseTemplate(model, template, function(err, body) {
      if(err) return console.log(err);
      var mailOptions = {
              from: "phillip@schejule.com",
              to: "phillipgburch@gmail.com",
              subject: "Hello world!",
              html: body,
              generateTextFromHTML: true
          }

          transport.sendMail(mailOptions);
    })
  },

  likedAnnotation: function(to, model, template) {
    this.canEmail(to, function() {
      if(!can) return;
      var transport = me._getTransport();
      me._parseTemplate(model, template, function(err, body) {
        if(err) return console.log(err);
        var mailOptions = {
          from: "LastVine Updates <notifications-noreply@lastvine.com>",
          to: (AppConfig.emailDisabled) ? 'phillipgburch@gmail.com' : to,
          subject: model.notification.actor.name + ' liked your interpretation',
          html: body,
          generateTextFromHTML: true
        }

        transport.sendMail(mailOptions);
      })
    })
  },
  newUserReport: function(user) {
    var me = this;
    var transport = me._getTransport();
    var mailOptions = {
      from: "LastVine Updates <notifications-noreply@lastvine.com>",
      to: 'phillipgburch@gmail.com',
      subject: 'LASTVINE: NEW USER ',
      html: user.name + ' Has Signed up for a lastvine account.',
      generateTextFromHTML: true
    }

    transport.sendMail(mailOptions);

  },

  newGroupReport: function(user, group) {
    var me = this;
    var transport = me._getTransport();
    var mailOptions = {
      from: "LastVine Updates <notifications-noreply@lastvine.com>",
      to: 'phillipgburch@gmail.com',
      subject: 'LASTVINE: NEW Group ',
      html: user.name + ' Has create a group on lastvine account. The groups Name is ' + group.name,
      generateTextFromHTML: true
    }

    transport.sendMail(mailOptions);
  },

  inviteEmail: function(to, model, template) {
    var me = this;
    this.canEmail(to, function(can) {
      if(!can) return;
      var transport = me._getTransport();
      me._parseTemplate(model, template, function(err, body) {
        if(err) return console.log(err);
        var mailOptions = {
          from: model.user.name + " <invitations@lastvine.com>",
          to: (AppConfig.emailDisabled) ? 'phillipgburch@gmail.com' : to,
          subject:'Join my Bible Study on LastVine',
          html: body,
          generateTextFromHTML: true
        }

        transport.sendMail(mailOptions);
      })
    });
  },

  // JOINED EMAIL
  joinedGroup: function(to, model, template) {
    var me = this;
    this.canEmail(to, function(can) {
      if(!can) return;
      var transport = me._getTransport();
      me._parseTemplate(model, template, function(err, body) {
        if(err) return console.log(err);
        var mailOptions = {
          from: "LastVine Notifications <notifications@lastvine.com>",
          to: (AppConfig.emailDisabled) ? 'phillipgburch@gmail.com' : to,
          subject: model.user.name + ' joined your Bible Study',
          html: body,
          generateTextFromHTML: true
        }

        transport.sendMail(mailOptions);
      })
    });
  },

  newPassage: function(to, model, template) {
    var me = this;
    this.canEmail(to, function(can) {
      if(!can) return;
      var transport = me._getTransport();
      me._parseTemplate(model, template, function(err, body) {
        if(err) return console.log(err);
        var mailOptions = {
          from: "LastVine Notifications <notifications@lastvine.com>",
          to: (AppConfig.emailDisabled) ? 'phillipgburch@gmail.com' : to,
          subject: model.passage.title + " is today's Bible Study",
          html: body,
          generateTextFromHTML: true
        }

        transport.sendMail(mailOptions);
      })
    });
  },

  commentedAnnotation: function(to, model, template) {
    var me = this;
    this.canEmail(to, function(can) {
      if(!can) return;
      var transport = me._getTransport();
      me._parseTemplate(model, template, function(err, body) {
        if(err) return console.log(err);
        var mailOptions = {
          from: "LastVine Updates <notifications-noreply@lastvine.com>",
          to: (AppConfig.emailDisabled) ? 'phillipgburch@gmail.com' : to,
          subject: model.notification.actor.name + ' commented on your interpretation',
          html: body,
          generateTextFromHTML: true
        }

        transport.sendMail(mailOptions);
      })
    })
  },

  newNote: function(to, model, template) {
    var me = this;
    this.canEmail(to, function(can) {
      if(!can) return;
      var transport = me._getTransport();
      me._parseTemplate(model, template, function(err, body) {
        if(err) return console.log(err);
        var mailOptions = {
          from: "LastVine Updates <notifications-noreply@lastvine.com>",
          to: (AppConfig.emailDisabled) ? 'phillipgburch@gmail.com' : to,
          subject: model.notification.actor.name + ' commented in ' + model.group.name,
          html: body,
          generateTextFromHTML: true
        }

        transport.sendMail(mailOptions);
      })
    })
  },

  recoverPassword: function(to, token) {
    var transport = this._getTransport();
    this._parseTemplate({token: token}, 'recoverpassword', function(err, body) {
      if(err) return console.log(err);
      var mailOptions = {
        from: "Forgot Password <noreply@lastvine.com>",
        to: (AppConfig.emailDisabled) ? 'phillipgburch@gmail.com' : to,
        subject: 'Reset your LastVine password.',
        html: body,
        generateTextFromHTML: true
      }

      transport.sendMail(mailOptions);
    })
  }
};

// Make class a singleton
module.exports = service;
//
//  main.m
//  Schejule
//
//  Created by Phillip Burch on 3/24/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "schejuleAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([schejuleAppDelegate class]));
    }
}

//
//  schejulePaymentController.h
//  Schejule
//
//  Created by Phillip Burch on 3/25/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "STPView.h"

@interface schejulePaymentController :  UIViewController <STPViewDelegate>

@property STPView* checkoutView;

- (IBAction)save:(id)sender;

@end

//
//  schejuleMasterViewController.m
//  Schejule
//
//  Created by Phillip Burch on 3/24/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleMasterViewController.h"

#import "schejuleDetailViewController.h"

@interface schejuleMasterViewController () {
    NSMutableArray *_objects;
}
@end

@implementation schejuleMasterViewController

- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        self.mapView.mapType = MKMapTypeStandard;
    self.mapView.showsUserLocation = YES;

    self.mapView.delegate = self;
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = userLocation.coordinate;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";
    
    [self.mapView addAnnotation:point];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

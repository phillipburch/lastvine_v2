//
//  schejuleMasterViewController.h
//  Schejule
//
//  Created by Phillip Burch on 3/24/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class schejuleDetailViewController;

@interface schejuleMasterViewController : UIViewController<MKMapViewDelegate>

@property (strong, nonatomic) schejuleDetailViewController *detailViewController;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

//
//  schejuleAppDelegate.h
//  Schejule
//
//  Created by Phillip Burch on 3/24/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface schejuleAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
